from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='DayTracker terminal',
    version='1.5.0',
    author='Alexandr Yushkevich',
    author_email='yushkevichaleks@yandex.ru',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'ReadMe.md')).read(),
    test_suite="tests.test_runner.get_test_suite",

    entry_points={
        'console_scripts':
            [
            'day = terminal_daytracker.tracker:main',
            'dayconfig = terminal_daytracker.config_maker:main'
            ]
        },

    include_package_data=True
)