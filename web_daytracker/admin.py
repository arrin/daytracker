from django.contrib import admin
from .models import (Task,
                     TaskStoryRecord,
                     TasksList,
                     UserProfile,
                     UserPermission,
                     TaskNotification,
                     Plan)


admin.site.register(Task)
admin.site.register(TaskStoryRecord)
admin.site.register(TasksList)
admin.site.register(UserPermission)
admin.site.register(TaskNotification)
admin.site.register(Plan)
admin.site.register(UserProfile)