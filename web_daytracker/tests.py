from django.test import TestCase
from datetime import timedelta
from web_daytracker.sqlite_storage import SQLiteStorage
from lib.entity import task
from web_daytracker import models
from lib.logger import disable_logger
from lib.data_storage import Storage
from lib import exceptions
from lib.tasks_manager import TasksManager


class StorageLogicTests(TestCase):
    def setUp(self):
        disable_logger()
        self.storage = SQLiteStorage()
        self.user = 'tester'
        models.UserProfile(id=self.user).save()
        self.tasks_manager = TasksManager(Storage(self.storage))


    def test_storage_setup(self):
        new_task = task.Task(title='added task', created='2018-12-12 12:00',
                             finish='2018-12-12 12:00', user_creator=self.user)
        self.storage = self.storage.upload_task(new_task)

    def test_base_task_creating(self):
        finish_time = '2018-12-12 12:00'
        task_id = self.tasks_manager.create_task(title='test', finish=finish_time, creator=self.user)
        task = self.tasks_manager.get_task(task_id)

        self.assertTrue(task.title == 'test')
        self.assertTrue(task.finish == finish_time)

    def test_try_cycle_subtask(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.set_subtask(task_id, another_task_id)
        try:
            self.tasks_manager.set_subtask(another_task_id, task_id)
        except exceptions.IncorrectRelations:
            pass
        except Exception:
            raise

    def test_set_subtask(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.set_subtask(task_id, another_task_id)
        task = self.tasks_manager.get_task(another_task_id)
        self.assertTrue(task.parent_task == task_id)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.parent_task is None)

    def test_update_task_finish_time(self):
        time = '2018-12-12 15:00'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.update_finish_time(task_id, time)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.finish == time)
        self.assertTrue(len(self.storage.get_task_field(task_id, notifications=True)) == 1)

    def test_remove_root_task(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        third_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.set_subtask(task_id, another_task_id)
        self.tasks_manager.set_subtask(task_id, third_task_id)

        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.remove_task(self.user, task_id)
        task = self.tasks_manager.get_task(another_task_id)
        self.assertTrue(task.parent_task is None)
        task = self.tasks_manager.get_task(third_task_id)
        self.assertTrue(task.parent_task is None)

    def test_split_subtask(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.set_subtask(task_id, another_task_id)
        self.tasks_manager.add_task(self.user, another_task_id)
        self.tasks_manager.split_tasks(self.user, task_id, another_task_id)

        task = self.tasks_manager.get_task(another_task_id)
        self.assertTrue(task.parent_task is None)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.parent_task is None)

    def test_add_next_task_weekdays(self):
        plan_id = self.tasks_manager.create_plan(user_id=self.user, title='test',
                    starting_date='2040-06-09 01:00', weekdays='0010010')
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        self.tasks_manager.set_task_properties(task_id, status=task.Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task_obj = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task_obj.finish == '2040-06-13 01:00')

        self.tasks_manager.set_task_properties(task_id, status=task.Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task_obj = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task_obj.finish == '2040-06-16 01:00')

    def test_add_next_task_timedelta(self):
        plan_id = self.tasks_manager.create_plan(user_id=self.user, title='test',
            starting_date='3000-12-12 01:00', time_delta=timedelta(hours=2))
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id

        self.tasks_manager.set_task_properties(task_id, status=task.Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task_obj = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task_obj.finish == '3000-12-12 03:00')

    def test_finish_tasks_with_subtasks(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        second_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.set_subtask(task_id, second_task_id)
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.add_task(self.user, second_task_id)
        self.tasks_manager.mark_task_finished(self.user, second_task_id)
        self.tasks_manager.mark_task_finished(self.user, task_id)
        task_obj = self.tasks_manager.get_task(task_id)
        self.assertTrue(task_obj.status == task.Status.FINISHED)

    def test_user_lists_adding(self):
        list_name = 'aaa'
        self.tasks_manager.storage.update_user_list(user_id=self.user, list_name=list_name, tasks=set())
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.add_task_to_list(self.user, list_name, task_id)
        user_list = self.tasks_manager.get_user_list(self.user, list_name)
        self.assertTrue(len(user_list) == 1)
        self.assertTrue(task_id in user_list)

    def test_add_task_to_list_without_access(self):
        list_name = 'aaa'
        self.tasks_manager.storage.update_user_list(user_id=self.user, list_name=list_name, tasks=set())
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        try:
            self.tasks_manager.add_task_to_list(user_id=self.user, list_name=list_name, task_id=task_id)
        except exceptions.PermissionDenied:
            pass
        except Exception:
            raise
        user_list = self.tasks_manager.get_user_list(self.user, list_name)
        self.assertTrue(len(user_list) == 0)
        self.assertFalse(task_id in user_list)

    def test_add_information_relations(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        second_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.add_task(self.user, second_task_id)
        self.tasks_manager.add_information_relations(self.user, task_id, second_task_id)
        self.assertEqual(len(self.storage.get_task_field(task_id=task_id, informations=True)), 1)
        self.assertEqual(self.storage.get_task_field(task_id, informations=True)[0], second_task_id)

    def test_remove_notification(self):
        time = '2018-12-12 12:00'
        task_id = self.tasks_manager.create_task(title='test', finish=time, creator=self.user)
        self.tasks_manager.remove_notification(task_id, time)

        self.assertFalse(len(self.storage.get_task_field(task_id, notifications=True)))
        self.assertFalse(time in self.storage.get_task_field(task_id, notifications=True))

    def test_get_nearest_notification(self):
        time = '2018-12-12 15:00'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 20:00', creator=self.user)
        self.tasks_manager.add_notification(task_id, time)
        note_time = self.tasks_manager.get_nearest_notification(task_id)
        self.assertTrue(note_time == time)

