"""
    SQLite storage implementation for DayTracker lib, need connection to django models database.

    Classes:
        SQLiteStorage - storage implementation

"""

from datetime import datetime, timedelta
from web_daytracker import models
from lib.storage.abstract_storage import AbstractStorage
from lib.entity.task import (Task,
                             TaskActionRecord,
                             OperationType)
from lib.entity.plan import Plan



class SQLiteStorage(AbstractStorage):
    def clear(self):
        pass

    def get_task(self, task_id):
        task = models.Task.objects.get(id=task_id)
        lib_task = Task(id=task.id, finish=datetime.strftime(task.finish, '%Y-%m-%d %H:%M'), status=task.status,
                         priority=task.priority, created=datetime.strftime(task.created, '%Y-%m-%d %H:%M'), title=task.title,
                         parent_task=task.parent_task.id if task.parent_task is not None else None,
                         user_creator=task.user_creator)
        return lib_task


    def upload_task(self, task):
        model_task = models.Task(id=task.id, finish=datetime.strptime(task.finish, '%Y-%m-%d %H:%M'), priority=task.priority,
                                 status=task.status, created=datetime.strptime(task.created, '%Y-%m-%d %H:%M'), title=task.title,
                                 parent_task=models.Task.objects.get(id=task.parent_task) if task.parent_task is not None else None,
                                 user_creator=models.UserProfile.objects.get(id=task.user_creator))
        model_task.save()


    def remove_task(self, task_id):
        task = models.Task.objects.get(id=task_id)
        task.delete()


    def get_task_field(self, task_id, history=False, notifications=False, informations=False):
        task = models.Task.objects.get(id=task_id)
        if history is True:
            history_records = models.TaskStoryRecord.objects.filter(task=task)
            history = [TaskActionRecord(time=datetime.strftime(record.time, '%Y-%m-%d %H:%M'), message=record.record)
                        for record in history_records]
            return sorted(history, key=lambda r: r.time, reverse=True)
        elif notifications is True:
            notifications_records = models.TaskNotification.objects.filter(task=task)
            notifications = [datetime.strftime(record.time, '%Y-%m-%d %H:%M')
                             for record in notifications_records]
            return sorted(notifications)
        elif informations is True:
            informations = [info_task.id for info_task in task.informations.all()]
            return informations
        return list()


    def update_task_field(self, task_id, history=None, notifications=None, informations=None):
        task = models.Task.objects.get(id=task_id)
        if notifications is not None:
            models.TaskNotification.objects.filter(task=task).delete()
            for note in notifications:
                n = models.TaskNotification(task=task, time=datetime.strptime(note, '%Y-%m-%d %H:%M'))
                n.save()

        if history is not None:
            models.TaskStoryRecord.objects.filter(task=task).delete()
            for rec in history:
                r = models.TaskStoryRecord(task=task, time=datetime.strptime(rec.time, '%Y-%m-%d %H:%M'), record=rec.message)
                r.save()

        if informations is not None:
            for info_id in informations:
                task.informations.add(models.Task.objects.get(id=info_id))


    def upload_plan(self, plan):
        model_plan = models.Plan(id=plan.id, weekdays=plan.weekdays, time_delta=plan.time_delta, repeat=plan.repeat, title=plan.title,
                                 last_added=datetime.strptime(plan.last_added, '%Y-%m-%d %H:%M') if plan.last_added is not None else None,
                                 extreme_date=plan.extreme_date if plan.extreme_date is not None else None,
                                 user_creator=models.UserProfile.objects.get(id=plan.user_id),
                                 last_added_id=models.Task.objects.get(id=plan.last_added_id) if plan.last_added_id is not None else None)
        model_plan.save()


    def get_plan(self, plan_id):
        plan = models.Plan.objects.get(id=plan_id)
        lib_plan = Plan(id=plan.id, weekdays=plan.weekdays, time_delta=plan.time_delta, repeat=plan.repeat, title=plan.title,
                        last_added=datetime.strftime(plan.last_added, '%Y-%m-%d %H:%M') if plan.last_added is not None else None,
                        extreme_date=datetime.strftime(plan.extreme_date, '%Y-%m-%d %H:%M') if plan.extreme_date is not None else None,
                        user_id=plan.user_creator.id,
                        last_added_id=plan.last_added_id.id if plan.last_added_id is not None else None)

        return lib_plan


    def remove_plan(self, plan_id):
        plan = models.Plan.objects.get(id=plan_id)
        plan.delete()


    def get_user_plans(self, user_id):
        if user_id is not None:
            plans = models.Plan.objects.filter(user_creator=user_id)
        else:
            plans = models.Plan.objects.all()
        return [plan.id for plan in plans]


    def get_user_list(self, user_id, list_name=None):
        if list_name is not None:
            user_list = models.TasksList.objects.get(name=list_name, user_owner=models.UserProfile.objects.get(id=user_id))
            return [task.id for task in user_list.tasks.all()]
        else:
            user_lists = models.TasksList.objects.filter(user_owner=models.UserProfile.objects.get(id=user_id))
            user_lists = [user_list.name for user_list in user_lists] if user_lists is not None else list()
            return user_lists


    def update_user_list(self, user_id, list_name, tasks=None):
        models.TasksList.objects.filter(name=list_name, user_owner=models.UserProfile.objects.get(id=user_id)).delete()
        if tasks is not None:
            user_list = models.TasksList(name=list_name, user_owner=models.UserProfile.objects.get(id=user_id))
            user_list.save()
            for task_id in tasks:
                user_list.tasks.add(models.Task.objects.get(id=task_id))


    def get_task_permissions(self, user_id, task_id):
        permissions = models.UserPermission.objects.filter(user=models.UserProfile.objects.get(id=user_id),
                                                           task=models.Task.objects.get(id=task_id))
        return [p.permission_type for p in permissions]


    def set_task_permissions(self, user_id, task_id, user_permissions):
        models.UserPermission.objects.filter(user=models.UserProfile.objects.get(id=user_id),
                                             task=models.Task.objects.get(id=task_id)).delete()
        for p in user_permissions:
            models.UserPermission(user=models.UserProfile.objects.get(id=user_id),
                                  task=models.Task.objects.get(id=task_id),
                                  permission_type=p).save()


    def get_filtered_tasks(self, user_reader=None, status=None, title=None, priority=None,
                            created_date=None, finish_date=None, user_creator=None, parent_task=-1):
        tasks = models.Task.objects.all()
        if status is not None:
            tasks = tasks.filter(status=status.value)
        if title is not None:
            tasks = tasks.filter(title=title)
        if priority is not None:
            tasks = tasks.filter(priority=priority.value)
        if created_date is not None:
            date = datetime.strptime(created_date, '%Y-%m-%d')
            tasks = tasks.filter(created__range=[date, date + timedelta(days=1)])
        if finish_date is not None:
            finish = datetime.strptime(finish_date, '%Y-%m-%d')
            tasks = tasks.filter(finish__range=[finish, finish + timedelta(days=1)])
        if parent_task != -1:
            tasks = tasks.filter(parent_task=models.Task.objects.get(id=parent_task))
        if user_creator is not None:
            tasks = tasks.filter(user_creator=models.UserProfile.objects.get(id=user_creator))
        if user_reader is not None:
            task_ids = list()
            for task in tasks:
                if OperationType.READ in self.get_task_permissions(user_reader, task.id):
                    task_ids.append(task.id)
        else:
            task_ids = [task.id for task in tasks]

        return task_ids