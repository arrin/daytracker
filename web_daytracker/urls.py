from django.conf.urls import url
from web_daytracker import views, forms

urlpatterns = [
    url(r'^$', views.main_page, name='home_page'),

    url(r'^tasks/$', views.tasks_page, name='tasks_list'),
    url(r'^tasks/new/$', views.add_task, name='add_task'),
    url(r'^tasks/finish/(?P<task_id>[0-9A-Fa-f]+)/$', views.finish_task),
    url(r'^tasks/remove/(?P<task_id>[0-9A-Fa-f]+)/$', views.remove_task),
    url(r'^tasks/archive/(?P<task_id>[0-9A-Fa-f]+)/$', views.archive_task),
    url(r'^tasks/details/(?P<task_id>[0-9A-Fa-f]+)/$', views.task_details),
    url(r'^tasks/filter/$', views.filter_tasks, name='filter'),
    url(r'^task/(?P<task_id>[0-9A-Fa-f]+)/remove/subtask/(?P<subtask_id>[0-9A-Fa-f]+)$', views.remove_subtask),
    url(r'^task/(?P<task_id>[0-9A-Fa-f]+)/add/subtask/$', views.add_subtask),
    url(r'^add_permission/(?P<task_id>[0-9A-Fa-f]+)$', views.add_permissions),
    url(r'^task/(?P<task_id>[0-9A-Fa-f]+)/remove/permissions/(?P<user_id>[a-zA-Z0-9_.-]+)$', views.remove_permissions),
    url(r'^tasks/(?P<task_id>[0-9A-Fa-f]+)/notifications/(?P<time>[0-9:-]+)$', views.remove_notification),
    url(r'^task/(?P<task_id>[0-9A-Fa-f]+)/notifications/add', views.add_notification),

    url(r'^join/$', forms.RegisterFormView.as_view()),
    url(r'^login.html$', forms.LoginFormView.as_view()),
    url(r'^logout/$', forms.LogoutView.as_view()),
    url(r'^accounts/login', forms.LoginFormView.as_view()),

    url(r'^profile/$', views.user_profile),
    url(r'^friends/$', views.user_friends, name='friends_page'),
    url(r'^friends/remove/(?P<friend_id>[a-zA-Z0-9_.-]+)$', views.remove_friend),

    url(r'^plans/$', views.user_plans, name='user_plans'),
    url(r'^plans/stop/(?P<plan_id>[0-9A-Fa-f]+)$', views.stop_plan),
    url(r'^plans/remove/(?P<plan_id>[0-9A-Fa-f]+)$', views.remove_plan),
    url(r'^plans/add/', views.add_plan),
    url(r'^plans/edit/(?P<plan_id>[0-9A-Fa-f]+)$', views.edit_plan),

    url(r'^lists/$', views.lists_page, name='lists_page'),
    url(r'^lists/add/$', views.add_list),
    url(r'^lists/remove/(?P<list_name>[a-zA-Z0-9_.-]+)$', views.remove_list),
    url(r'^lists/open/(?P<list_name>[a-zA-Z0-9_.-]+)$', views.list_details),
    url(r'^lists/(?P<list_name>[a-zA-Z0-9_.-]+)/remove/task/(?P<task_id>[0-9A-Fa-f]+)$', views.remove_task_from_list),
    url(r'^lists/(?P<list_name>[a-zA-Z0-9_.-]+)/add/task/$', views.add_task_to_list)

]
