from uuid import uuid4
from lib.tasks_manager import TasksManager
from web_daytracker.sqlite_storage import SQLiteStorage
from lib.data_storage import Storage
from lib.logger import disable_logger
from web_daytracker.models import *


def get_manager():
    storage = Storage(SQLiteStorage())
    manager = TasksManager(storage, gen_id=lambda: str(uuid4().hex)[-7:])
    disable_logger()
    return manager


def update_tasks_status(func):
    def updater(*args, **kwargs):
        get_manager().update_tasks_status()
        get_manager().update_plans()
        return func(*args, **kwargs)
    return updater


def get_tasks_models(task_ids):
    return [Task.objects.get(id=task_id) for task_id in task_ids]


def get_task_priorities():
    return [(len(Priority), 'all')] + [(p.value, p.name.lower()) for p in Priority]


def get_task_statuses():
    return [(len(Status), 'all')] + [(s.value, s.name.lower()) for s in Status]


def get_task_operation_types():
    return [p for p in OperationType]