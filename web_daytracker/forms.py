from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.forms import widgets
from web_daytracker.models import UserProfile, Task, Plan
from django.contrib.auth.models import User
from django.forms import ModelForm, DateTimeInput, Form
from django import forms
from web_daytracker.logic_connector import get_task_statuses, get_task_priorities
from web_daytracker.widgets import TimeDeltaSelectorWidget


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/user_tasks/"
    template_name = "join.html"

    def form_valid(self, form):
        form.save()
        name = form.cleaned_data['username']
        UserProfile(id=name, user_auth=User.objects.get(username=name)).save()
        super(RegisterFormView, self).form_valid(form)
        login(self.request, User.objects.get(username=name))
        return HttpResponseRedirect("/tasks/")


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = "/tasks/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        super(LoginFormView, self).form_valid(form)
        return HttpResponseRedirect("/tasks/")


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class CustomDateTimeInput(DateTimeInput):
    input_type = 'datetime-local'


class TaskAdding(ModelForm):
    class Meta:
        model = Task
        fields = ('title', 'finish', 'priority')
        widgets = {
            'finish': forms.DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM'}),
        }


class TaskEditing(ModelForm):
    class Meta:
        model = Task
        fields = ('title', 'finish', 'priority', 'status')
        widgets = {
            'finish':forms.DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM'}),
        }

class FilterForm(Form):
    title = forms.CharField(max_length=50, required=False)
    finish = forms.CharField(label='Finish date', widget=forms.DateInput(attrs={'type': 'date',
                                                                                'placeholder': 'YYYY-MM-DD'}),
                             required=False)
    priority = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select-wrapper'}),
                                 choices=get_task_priorities(), required=False)
    status = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select-wrapper'}),
                                 choices=get_task_statuses(),
                                 required=False)
    created = forms.CharField(label='Creation date', widget=forms.DateInput(attrs={'type': 'date',
                                                                                   'placeholder': 'YYYY-MM-DD'}),
                              required=False)
    created_by_me = forms.BooleanField(widget=forms.CheckboxInput(), required=False, label='Only created by me')


class PlanEditing(ModelForm):
    class Meta:
        model = Plan
        fields = ('title', 'weekdays', 'time_delta', 'repeat', 'extreme_date')
        widgets = {
            'time_delta': TimeDeltaSelectorWidget(),
            'extreme_date': forms.DateTimeInput(attrs={'placeholder': 'YYYY-MM-DD HH:MM'}),
        }
        help_texts = {
            'time_delta': 'Days and hours for task adding cycle. If you set both weekdays and time_delta, only weekdays will work',
        }
    starting_date = forms.CharField(label='Starting date', widget=forms.DateTimeInput(attrs={'type': 'datetime-local',
                                                                                             'placeholder': 'YYYY-MM-DD HH:MM'}),
                                    required=False)

