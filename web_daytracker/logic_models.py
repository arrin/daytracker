from web_daytracker.models import UserPermission, Task, UserProfile, Plan


def get_task_users(task_id):
    permissions = dict()
    for permission in UserPermission.objects.filter(task=Task.objects.get(id=task_id)):
        user = permission.user.id
        if user in permissions:
            permissions[user].append(permission.permission_type)
        else:
            permissions[user] = [permission.permission_type]
    return permissions


def get_user_friends(user_id):
    return [friend.id for friend in UserProfile.objects.get(id=user_id).friends.all()]


def get_plans_models(plans_ids):
    return [Plan.objects.get(id=id) for id in plans_ids]
