import copy
import re
from datetime import datetime, timedelta
from collections import namedtuple
from django.forms.widgets import HiddenInput
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from web_daytracker.models import *
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone
from web_daytracker.logic_connector import (get_manager, get_tasks_models,
                                            get_task_operation_types, update_tasks_status)
from web_daytracker.logic_models import get_task_users, get_user_friends, get_plans_models
from django.shortcuts import redirect
from web_daytracker import forms
from web_daytracker.exceptions_handler import exceptions_handler


@update_tasks_status
@exceptions_handler
def main_page(request):
    template = loader.get_template('main_page.html')
    context = {'user_status': request.user.username}
    return HttpResponse(template.render(context))

@update_tasks_status
@exceptions_handler
@login_required
def tasks_page(request, tasks=None):
    manager = get_manager()
    template = loader.get_template('user_tasks.html')
    user_id = request.user.username
    context = {'tasks': (get_tasks_models(tasks) if tasks is not None
                               else get_tasks_models(manager.storage.get_filtered_tasks(user_reader=user_id))),
               'user_status': request.user.username,
               'is_filtered': False if tasks is None else True}
    return HttpResponse(template.render(context))

@update_tasks_status
@exceptions_handler
@login_required
def finish_task(request, task_id):
    user_id = request.user.username
    get_manager().mark_task_finished(user_id, task_id)
    return redirect('/tasks/')


@exceptions_handler
@login_required
def remove_task(request, task_id):
    manager = get_manager()
    user_id = request.user.username
    get_manager().remove_task(user_id, task_id)
    return redirect('/tasks/')


@exceptions_handler
@login_required
def archive_task(request, task_id):
    user_id = request.user.username
    get_manager().mark_task_archived(user_id, task_id)
    return redirect('/tasks/')


@exceptions_handler
@login_required
def add_task(request):
    if request.method == "POST":
        form = forms.TaskAdding(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            manager = get_manager()
            user_id = request.user.username
            task_id = manager.create_task(title=task.title, finish=datetime.strftime(task.finish, '%Y-%m-%d %H:%M'),
                                            creator=user_id)
            manager.add_task(user_id, task_id)
            manager.set_task_field(user_id, task_id, priority=task.priority)
            return redirect('/tasks/')
    else:
        form = forms.TaskAdding()
    return render(request, 'add_task.html', {'form': form, 'user_status': request.user.username})


@exceptions_handler
@login_required
def user_friends(request):
    if request.method == "POST":
        friend_id = request.POST['name']
        if UserProfile.objects.filter(id=friend_id).exists():
            UserProfile.objects.get(id=request.user.username).friends.add(UserProfile.objects.get(id=friend_id))
            UserProfile.objects.get(id=friend_id).friends.add(UserProfile.objects.get(id=request.user.username))
    friends = get_user_friends(request.user.username)
    return render(request, 'friends.html', {'friends': friends, 'user_status': request.user.username})


@exceptions_handler
@login_required
def remove_friend(request, friend_id):
    UserProfile.objects.get(id=request.user.username).friends.remove(UserProfile.objects.get(id=friend_id))
    UserProfile.objects.get(id=friend_id).friends.remove(UserProfile.objects.get(id=request.user.username))
    return redirect('/friends/')


@exceptions_handler
@login_required
def remove_subtask(request, task_id, subtask_id):
    manager = get_manager()
    user_id = request.user.username
    if manager.validate_permissions(request.user.username, task_id, OperationType.EDIT):
        manager.split_tasks(user_id, task_id, subtask_id)
    return redirect('/tasks/details/{}#subtasks'.format(task_id))


@exceptions_handler
@login_required
def add_subtask(request, task_id):
    manager = get_manager()
    user_id = request.user.username
    if request.method == "POST" and manager.validate_permissions(user_id, task_id, OperationType.EDIT):
        subtask_id = request.POST.get('subtask')
        if subtask_id:
            manager.add_subtask(user_id, task_id, subtask_id)
    return redirect('/tasks/details/{}#subtasks'.format(task_id))


@exceptions_handler
@login_required
def add_permissions(request, task_id):
    manager = get_manager()
    user_id = request.user.username
    if request.method == "POST" and manager.validate_permissions(user_id, task_id, OperationType.EDIT):
        user_id = request.POST.get('friend_id')
        READ = '0'
        for permissions in request.POST.getlist('operations'):
            if not READ in permissions:
                permissions += READ
            manager.add_task(user_id, task_id, [OperationType(int(p)) for p in permissions])
    return redirect('/tasks/details/{}#users'.format(task_id))


@exceptions_handler
@login_required
def remove_permissions(request, task_id, user_id):
    manager = get_manager()
    if manager.validate_permissions(request.user.username, task_id, OperationType.EDIT):
        UserPermission.objects.filter(task=Task.objects.get(id=task_id),
                                      user=UserProfile.objects.get(id=user_id)).delete()
    return redirect('/tasks/details/{}#users'.format(task_id))


@exceptions_handler
@login_required
def remove_notification(request, task_id, time):
    manager = get_manager()
    time = time[:10] + ' ' + time[11:13] + ':' + time[-2:]
    manager.remove_notification(task_id=task_id, time=time)
    return redirect('/tasks/details/{}#notifications'.format(task_id))


@exceptions_handler
@login_required
def add_notification(request, task_id):
    if request.method == "POST":
        time = request.POST.get('time')
        if time:
            time = re.sub('T', ' ', time)
            get_manager().add_task_notification(user_id=request.user.username, task_id=task_id, notification=time)
    return redirect('/tasks/details/{}#notifications'.format(task_id))


@update_tasks_status
@exceptions_handler
@login_required
def task_details(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    manager = get_manager()
    user_id = request.user.username
    old_title = copy.copy(task.title)
    old_finish = copy.copy(datetime.strftime(task.finish, '%Y-%m-%d %H:%M'))
    old_status = copy.copy(task.status)
    old_priority = copy.copy(task.priority)

    if request.method == "POST":
        form = forms.TaskEditing(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            title = task.title if task.title != old_title else None
            finish = datetime.strftime(task.finish, '%Y-%m-%d %H:%M')
            finish = finish if finish != old_finish else None
            status = task.status if task.status != old_status else None
            priority = task.priority if task.priority != old_priority else None
            manager.set_task_field(user_id=user_id, task_id=task.id, finish_time=finish,
                                    title=title, priority=priority, status=status)
            return redirect('/tasks/details/{}'.format(task_id))

    else:
        task_form = forms.TaskEditing(instance=task)

    return render(request, 'task_details.html', {'user_status': request.user.username,
                                                 'task_id': task_id,
                                                 'subtasks': get_tasks_models(manager.storage.get_filtered_tasks(user_reader=user_id,
                                                                                                                 parent_task=task_id)),
                                                 'user_tasks': get_tasks_models((manager.storage.get_filtered_tasks(user_reader=user_id))),
                                                 'users': get_task_users(task_id),
                                                 'user_friends': get_user_friends(user_id),
                                                 'permissions': get_task_operation_types(),
                                                 'task_form': task_form,
                                                 'history': manager.storage.get_task_field(task_id=task_id, history=True),
                                                 'notifications': manager.storage.get_task_field(task_id=task_id, notifications=True),
                                                 'task_creator': task.user_creator.id})


@exceptions_handler
@login_required
def filter_tasks(request):
    if request.method == "POST":
        form = forms.FilterForm(request.POST)
        if form.is_valid():
            user_id = request.user.username
            finish_date = form.cleaned_data['finish'] if form.cleaned_data['finish'] else None
            created_date = form.cleaned_data['created'] if form.cleaned_data['created'] else None
            user_creator = user_id if form.cleaned_data['created_by_me'] else None
            title = user_id if form.cleaned_data['title'] else None
            status = form.cleaned_data['status']
            if status == '4':   # rm
                status = None
            priority = form.cleaned_data['priority']
            if priority == '3': # rm
                priority = None

            tasks_ids = get_manager().storage.get_filtered_tasks(user_reader=user_id, finish_date=finish_date, title=title,
                                                                 user_creator=user_creator, status=status, priority=priority,
                                                                 created_date=created_date)
            return tasks_page(request, tasks_ids)
    else:
        form = forms.FilterForm()
    return render(request, 'tasks_filter.html', {'form': form, 'user_status': request.user.username})

@update_tasks_status
@exceptions_handler
@login_required
def user_plans(request):
    manager = get_manager()
    user_id = request.user.username
    plans_ids = manager.storage.get_plans(user_id)
    return render(request, 'user_plans.html', {'plans': get_plans_models(plans_ids),
                                               'user_status': request.user.username})


@exceptions_handler
@login_required
def stop_plan(request, plan_id):
    manager = get_manager()
    plan = manager.storage.get_plan(plan_id)
    plan.repeat = 0
    manager.storage.update_plan(plan_id, plan)
    return redirect('/plans/')


@exceptions_handler
@login_required
def remove_plan(request, plan_id):
    manager = get_manager()
    manager.remove_plan(request.user.username, plan_id)
    return redirect('/plans/')


@exceptions_handler
@login_required
def add_plan(request):
    manager = get_manager()
    user_id = request.user.username
    if request.method == "POST":
        form = forms.PlanEditing(request.POST)
        if form.is_valid():
            plan = form.save(commit=False)
            starting_date = form.cleaned_data['starting_date']
            if starting_date:
                starting_date = re.sub('T', ' ', starting_date)
            else:
                starting_date = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d %H:%M')
            manager.create_plan(user_id=user_id, title=plan.title, weekdays=plan.weekdays, extreme_date=plan.extreme_date,
                                repeat=plan.repeat, time_delta=plan.time_delta,
                                starting_date=starting_date)
            return redirect('/plans/')
    else:
        form = forms.PlanEditing()
    return render(request, 'edit_plan.html', {'form': form, 'user_status': request.user.username})


@exceptions_handler
@login_required
def edit_plan(request, plan_id):
    plan = get_object_or_404(Plan, id=plan_id)
    if request.method == "POST":
        form = forms.PlanEditing(request.POST, instance=plan)
        if form.is_valid():
            plan = form.save(commit=False)
            if plan.weekdays and plan.time_delta:
                plan.time_delta = None
            plan.save()
            return redirect('/plans/')
    else:
        form = forms.PlanEditing(instance=plan)
        form.fields['starting_date'].widget = HiddenInput()
    return render(request, 'edit_plan.html', {'form': form, 'user_status': request.user.username})


@exceptions_handler
@login_required
def lists_page(request):
    manager = get_manager()
    user_id = request.user.username
    lists = manager.storage.get_user_lists_names(user_id)

    return render(request, 'user_lists.html', {'user_status': request.user.username, 'lists': lists})


@exceptions_handler
@login_required
def remove_list(request, list_name):
    manager = get_manager()
    user_id = request.user.username
    list_name = re.sub('-', ' ', list_name)
    manager.storage.update_user_list(user_id, list_name)

    return redirect('/lists/')


@exceptions_handler
@login_required
def add_list(request):
    if request.method == "POST":
        user_id = request.user.username
        list_name = request.POST.get('list_name')
        if list_name:
            get_manager().storage.update_user_list(user_id, list_name, list())

    return redirect('/lists/')


@exceptions_handler
@login_required
def list_details(request, list_name):
    manager = get_manager()
    user_id = request.user.username
    list_name = re.sub('-', ' ', list_name)
    task_ids = manager.get_user_list(user_id, list_name)
    tasks = get_tasks_models(task_ids)
    return render(request, 'list_details.html', {'user_status': request.user.username,
                                                 'tasks': tasks,
                                                 'list_name': list_name,
                                                 'user_tasks': get_tasks_models((manager.storage.
                                                                                 get_filtered_tasks(user_reader=user_id)))})


@exceptions_handler
@login_required
def remove_task_from_list(request, list_name, task_id):
    manager = get_manager()
    user_id = request.user.username
    list_name = re.sub('-', ' ', list_name)
    manager.remove_task_from_list(user_id=user_id, list_name=list_name, task_id=task_id)
    return redirect('/lists/open/{}'.format(re.sub(' ', '-', list_name)))


@exceptions_handler
@login_required
def add_task_to_list(request, list_name):
    manager = get_manager()
    user_id = request.user.username
    list_name = re.sub('-', ' ', list_name)
    if request.method == "POST":
        task_id = request.POST.get('task')
        if task_id:
            manager.add_task_to_list(user_id=user_id, list_name=list_name, task_id=task_id)
    return redirect('/lists/open/{}'.format(re.sub(' ', '-', list_name)))


@update_tasks_status
@exceptions_handler
@login_required
def user_profile(request):
    manager = get_manager()
    user_id = request.user.username
    Notification = namedtuple('Notification', ['time', 'task_name', 'task_id'])
    notifications = list()
    for task_id in manager.storage.get_filtered_tasks(user_reader=user_id, status=Status.ACTIVE):
        time = manager.get_nearest_notification(task_id)
        if time:
            notifications.append(Notification(time=time, task_name=manager.get_task(task_id).title,
                                              task_id=task_id))

    NOTIFICATIONS_COUNT = 5
    return render(request, 'user_profile.html', {'user_status': request.user.username,
                                                 'notifications': sorted(notifications)[:NOTIFICATIONS_COUNT],
                                                 'active': len(manager.storage.get_filtered_tasks(user_reader=user_id,
                                                                                                  status=Status.ACTIVE)),
                                                 'finished': len(manager.storage.get_filtered_tasks(user_reader=user_id,
                                                                                                  status=Status.FINISHED)),
                                                 'failed': len(manager.storage.get_filtered_tasks(user_reader=user_id,
                                                                                                  status=Status.FAILED)),
                                                 'archived': len(manager.storage.get_filtered_tasks(user_reader=user_id,
                                                                                                  status=Status.ARCHIVED))
                                                 })