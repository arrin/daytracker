from lib import exceptions
from django.http import HttpResponse, Http404
from django.template import loader


def exceptions_handler(func):
    def execute(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except exceptions.PermissionDenied as e:
            template = loader.get_template('error_page.html')
            context = {'exception': e}
            return HttpResponse(template.render(context))
        except exceptions.ImpossibleToFinish as e:
            template = loader.get_template('error_page.html')
            context = {'exception': e}
            return HttpResponse(template.render(context))
        except exceptions.IncorrectRelations as e:
            template = loader.get_template('error_page.html')
            context = {'exception': e}
            return HttpResponse(template.render(context))
        except exceptions.ListNotFound as e:
            template = loader.get_template('error_page.html')
            context = {'exception': e}
            return HttpResponse(template.render(context))
        except exceptions.PlanNotFound as e:
            template = loader.get_template('error_page.html')
            context = {'exception': e}
            return HttpResponse(template.render(context))
        except exceptions.TaskNotFound as e:
            template = loader.get_template('error_page.html')
            context = {'exception': e}
            return HttpResponse(template.render(context))
        except Exception:
            raise Http404

    return execute

