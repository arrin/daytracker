import re
from datetime import timedelta
from django.forms import widgets


class TimeDeltaSelectorWidget(widgets.MultiWidget):
    def __init__(self, attrs=None):
        _widgets = (
            widgets.NumberInput(attrs=attrs),
            widgets.NumberInput(attrs=attrs),
        )
        super(TimeDeltaSelectorWidget, self).__init__(_widgets, attrs)


    def decompress(self, value):
        if value:
            vals = re.findall(r"[\d']+", value)
            return [int(vals[0]), int(vals[1])]
        return [None, None]


    def format_output(self, rendered_widgets):
        return ''.join(rendered_widgets)


    def value_from_datadict(self, data, files, name):
        datelist = [
            widget.value_from_datadict(data, files, name + '_%s' % i)
            for i, widget in enumerate(self.widgets)]
        try:
            days = int(datelist[0]) if datelist[0] else 0
            hours = int(datelist[1]) if datelist[1] else 0
            if days >= 0 and hours >= 0 and days + hours != 0:
                delta = timedelta(days=days, hours=hours,)
            else:
                delta = ''
        except ValueError:
            return ''
        else:
            return delta