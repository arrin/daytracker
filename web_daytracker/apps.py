from django.apps import AppConfig


class DaytrackConfig(AppConfig):
    name = 'web_daytracker'
