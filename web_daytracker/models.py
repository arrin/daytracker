from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.db import models
from enumfields import EnumField
from lib.entity.task import (Status,
                             Priority,
                             OperationType)


class Task(models.Model):
    id = models.CharField(max_length=16, primary_key=True)
    title = models.CharField(max_length=128)
    created = models.DateTimeField(null=True)
    finish = models.DateTimeField(null=True)
    priority = EnumField(Priority)
    status = EnumField(Status)
    parent_task = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL)
    informations = models.ManyToManyField('self', blank=True)
    user_creator = models.ForeignKey('UserProfile', null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return '{0.title}'.format(self)


class TasksList(models.Model):
    name = models.CharField(max_length=32)
    user_owner = models.ForeignKey('UserProfile')
    tasks = models.ManyToManyField('Task', blank=True)

    def __str__(self):
        return self.name


class TaskNotification(models.Model):
    time = models.DateTimeField()
    task = models.ForeignKey('Task')

    def __str__(self):
        return '{} notification'.format(self.task.title)


class TaskStoryRecord(models.Model):
    time = models.DateTimeField()
    record = models.CharField(max_length=64)
    task = models.ForeignKey('Task', blank=True, null=True)

    def __str__(self):
        return self.record


class UserPermission(models.Model):
    permission_type = EnumField(OperationType)
    task = models.ForeignKey('Task', blank=True, null=True)
    user = models.ForeignKey('UserProfile', blank=True, null=True)

    def __str__(self):
        return 'user {0} - task {1}'.format(self.user.id, self.task.title)


class Plan(models.Model):
    id = models.CharField(primary_key=True, max_length=16)
    weekdays = models.CharField(validators=[RegexValidator(regex='[01]{7}', message='Weekdays must contains 0 or 1 and has lenght 7')],
                                max_length=7, blank=True, null=True)
    time_delta = models.DurationField(blank=True, null=True)
    last_added = models.DateTimeField(blank=True, null=True)
    title = models.CharField(max_length=128)
    repeat = models.IntegerField(blank=True, null=True)
    extreme_date = models.DateTimeField(blank=True, null=True)
    user_creator = models.ForeignKey('UserProfile')
    last_added_id = models.OneToOneField('Task', blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.title


class UserProfile(models.Model):
    user_auth = models.OneToOneField(User, null=True, blank=True)
    id = models.CharField(max_length=64, primary_key=True)
    friends = models.ManyToManyField('UserProfile', blank=True)

    def __str__(self):
        return self.id
