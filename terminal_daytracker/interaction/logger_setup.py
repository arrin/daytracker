import configparser
import logging
import sys
import os
from lib.logger import get_logger
from terminal_daytracker.config_maker import ConfigFieldsNames


def init_logger(config_path):
    """Logger initialization from config.

    Args:
        config_path: path to config
    """

    logger = get_logger()

    if os.path.exists(os.path.expanduser(config_path)):
        config = configparser.ConfigParser()
        config.read(os.path.expanduser(config_path))
        if (config[ConfigFieldsNames.NAME][ConfigFieldsNames.LOGGING] == ConfigFieldsNames.CONSOLE_MODE):
            consoleHandler = logging.StreamHandler(stream=sys.stdout)
            logger.addHandler(consoleHandler)
            logger.setLevel(logging.DEBUG)
        else:
            f = os.path.expanduser('{}/log'
                .format(config[ConfigFieldsNames.NAME][ConfigFieldsNames.PATH]))
            fileHandler = logging.FileHandler(f)
            logger.addHandler(fileHandler)
            logger.setLevel(logging.DEBUG)
    else:
        consoleHandler = logging.StreamHandler(stream=sys.stdout)
        logger.addHandler(consoleHandler)
        logger.setLevel(logging.DEBUG)
