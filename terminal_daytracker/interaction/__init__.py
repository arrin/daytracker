"""
    This package includes user console interaction for terminal_daytracker.

    Packages:
        parsers - package with argument parsers.
    
    Modules:
        output - user console output
        user_loading - module for user loading from local files
        logger_setup - provides functios for logger configuration
        storage_setup - provides functios for storage configuration

"""