"""
    Provides functions for console output tasks, groups and plans.

"""

from termcolor import colored
from lib import data_storage
from lib.entity.task import Status


def print_list_tasks(user_id, list_name, data_storage):
    tasks = data_storage.get_user_list(user_id, list_name)
    print('{}:'.format(list_name))
    for task_id in tasks:
        print_task_info(task_id, data_storage)


def print_detailed_task_info(task_id, data_storage, task_tree):
    print_task_info(task_id, data_storage)
    
    print('Task history:')
    history = data_storage.get_task_field(task_id, history=True)
    for record in reversed(history):
        print('    {}'.format(record))

    if len(task_tree) > 1:
        print('Subtasks:')
        for vrt in task_tree:
            if vrt.depth != 0:
                print_task_info(vrt.task_id, data_storage, tab=vrt.depth)

    informations = data_storage.get_task_field(task_id, informations=True)
    if len(informations):
        print('Information relations:')
        for id in informations:
            print_task_info(id, data_storage, 1)


def print_task_info(task_id, data_storage, tab=0):
    task = data_storage.get_task(task_id)
    output = (('    ' * tab + ('| ' if tab != 0 else '')
                + '{0.id}\t{0.title:>16.16}  ({0.finish})  {1} priority').
        format(task, task.priority.name.lower()))

    if task.status == Status.FINISHED:
        print(colored(output, 'green'))
    elif task.status == Status.FAILED:
        print(colored(output, 'red'))
    elif task.status == Status.ARCHIVED:
        print(colored(output, 'blue'))
    else:
        print(output)


def print_plan_info(plan_id, data_storage):
    plan = data_storage.get_plan(plan_id)
    if plan.days is None:
        mask = plan.time_delta
    else:
        mask = plan.days

    print('{0.id}\t{0.title:>25.25}   last added task: {0.last_added}  mask: {1}'.
            format(plan, mask))