import os
import configparser
import pathlib
from terminal_daytracker.config_maker import ConfigFieldsNames
from lib.storage.json_storage import JSONStorage
from lib.data_storage import Storage


def init_storage(config_path):
    """
        Init library storage.

        Args:
            config_path: path for config
        
    """

    config = configparser.ConfigParser()
    config.read(os.path.expanduser(config_path))
    path = os.path.expanduser(config[ConfigFieldsNames.NAME][ConfigFieldsNames.PATH])
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    return Storage(JSONStorage(path))