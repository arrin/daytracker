"""
    Provides function for loading user from local file. Uses app config.
    
"""

import os
import configparser
from uuid import uuid4
from lib import exceptions
from lib.logger import get_logger
from terminal_daytracker.config_maker import ConfigFieldsNames
from lib.entity.user_lists import UserLists


def load_user(config_path):
    config = configparser.ConfigParser()
    config.read(os.path.expanduser(config_path))

    user_id = config[ConfigFieldsNames.NAME][ConfigFieldsNames.USER]
    return user_id if user_id != 'No user' else None


def change_user(user_id, config_path, manager):
    config = configparser.ConfigParser()
    config.read(os.path.expanduser(config_path))

    config[ConfigFieldsNames.NAME][ConfigFieldsNames.USER] = user_id
    with open(os.path.expanduser(config_path), 'w') as f:
        config.write(f)
    if user_id != 'No user':
        manager.storage.init_user_lists(user_id, UserLists(id=user_id))
        get_logger().info('changed to user {0}'.format(user_id))
    else:
        get_logger().info('{} signed out'.format(user_id))


def check_config_dir(config_path):
    """
        Check config dir. Create it if dir doesn't exist.

    """

    if not os.path.exists(os.path.expanduser(config_path)):
        print('Config not found. Create config and retry.')
        return False

    config = configparser.ConfigParser()
    config.read(os.path.expanduser(config_path))
    app_path = config[ConfigFieldsNames.NAME][ConfigFieldsNames.PATH]
    if not os.path.exists(os.path.expanduser(app_path)):
        os.mkdir(os.path.expanduser(app_path))

    return True
