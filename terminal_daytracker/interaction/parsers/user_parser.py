from terminal_daytracker.interaction import user_loading
from terminal_daytracker.interaction.parsers.exceptions_wrapper import library_exceptions_handler

def init_parser(subparsers):
    user_parser = subparsers.add_parser("user", 
        help='users')
    user_parser.add_argument('-s', '--signin', nargs=1, 
        help='sign in as user by user_id')
    user_parser.add_argument('-w', '--who', action='store_true', 
        help='show current user name')
    user_parser.add_argument('-e', '--exit', action='store_true', 
        help='sign out')
    user_parser.set_defaults(which='users')


@library_exceptions_handler
def parse_args(args, config_path, manager):
    if args.which == 'users' and args.signin:
        user_loading.change_user(args.signin[0], config_path, manager)

    user_id = user_loading.load_user(config_path)
    if args.which == 'users' and args.who:
        print('current user: {0}'.format(user_id))

    if args.which == 'users' and args.exit:
        user_loading.change_user('No user', config_path, manager)

    return user_id
    
