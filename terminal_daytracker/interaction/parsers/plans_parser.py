# -*- coding: utf-8 -*-
from re import search
from datetime import datetime, timedelta
from terminal_daytracker.interaction import output
from lib import data_storage
from terminal_daytracker.interaction.parsers.exceptions_wrapper import library_exceptions_handler


def init_parser(subparsers):
    plan_parser = subparsers.add_parser("plans", help='allow to work with scheduler that can add tasks by some plan', 
        description='lan can add tasks with a period or by days of the week')

    plan_parser.add_argument(
        '-l', '--list', action='store_true', dest='plans_list',
            help='list of user plans')
    plan_parser.add_argument(
        '-r', '--remove', nargs=1, help='remove plan with some hash')
    plan_parser.set_defaults(which='plans')
    
    add_subparser = plan_parser.add_subparsers(
        help='allow to add plans')
    template_add_parser = add_subparser.add_parser("add")
    template_add_parser.set_defaults(which='add_plans')
    template_add_parser.add_argument('title', help='title for tasks in the template')
    template_add_parser.add_argument('starting_time', 
        help='time for the first plan task, format \'YYYY-MM-DD HH:MM\'',
        type=lambda d:datetime.strptime(d, '%Y-%m-%d %H:%M'))
    
    template_add_parser.add_argument('-w', '--weekdays', nargs=1, 
        help='scheduler will add task on the concrete weekdays, format: 7 symbols 0(off) or 1(on)',
        type=lambda s:search('[01]{7}', s).group(0))

    template_add_parser.add_argument('-d', '--days', nargs=1, 
        help='scheduler will add task every d days', type=int)
    template_add_parser.add_argument('-hr', '--hours', nargs=1, 
        help='scheduler will add task every h hours', type=int)

    template_add_parser.add_argument('-r', '--repeat', nargs=1, 
        help='set number of repetitions', type=int)
    template_add_parser.add_argument('-u', '--until', nargs=1, 
        help='last date for plans tasks, format \'YYYY-MM-DD HH:MM\'',
        type=lambda d:datetime.strptime(d, '%Y-%m-%d %H:%M'))


@library_exceptions_handler
def parse_args(args, user_id, manager):
    plans_parse(args, user_id, manager)
    adding_plans_parse(args, user_id, manager)
    

def plans_parse(args, user_id, manager):
    if args.which != 'plans':
        return

    if args.plans_list:
        plans = manager.storage.get_plans(user_id)
        for plan_id in plans:
            output.print_plan_info(plan_id, manager.storage)

    if args.remove:
        manager.remove_plan(user_id, args.remove[0])


def adding_plans_parse(args, user_id, manager):
    if args.which != 'add_plans':
        return

    if args.repeat:
        repeat = args.repeat[0]
    else:
        repeat = None

    if args.until:
        extreme_date = args.until[0]
    else:
        extreme_date = None

    if args.weekdays:
        manager.create_plan(user_id=user_id, title=args.title, 
                    starting_date=args.starting_time.strftime('%Y-%m-%d %H:%M'),
                    weekdays=args.weekdays[0], repeat=repeat, extreme_date=extreme_date)

    elif args.days or args.hours:
        days = args.days[0] if args.days is not None else 0
        hours = args.hours[0] if args.hours is not None else 0

        delta = timedelta(days=days, hours=hours)
        manager.create_plan(user_id=user_id, title=args.title,  
                starting_date=args.starting_time.strftime('%Y-%m-%d %H:%M'),
                time_delta=delta, repeat=repeat, extreme_date=extreme_date)
    else:
        print('Days settings not found')
        return