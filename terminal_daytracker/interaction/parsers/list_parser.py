# -*- coding: utf-8 -*-
import logging
from interaction import output
from lib import exceptions
from terminal_daytracker.interaction.parsers.exceptions_wrapper import library_exceptions_handler


def init_parser(subparsers):
    list_parser = subparsers.add_parser("list", description='concrete list handling',
        help='allow to handle single user list, task adding and removing')

    list_parser.add_argument('list_name', help='name of editing list')
    list_parser.add_argument('-a', '--add', nargs=1, 
        help='add task to list, task id')
    list_parser.add_argument('-r', '--remove', nargs=1, 
        help='remove task from list, task id')
    list_parser.add_argument('-l', '--list', action='store_true', 
        help='show tasks on concrete list')
    list_parser.set_defaults(which='list')


@library_exceptions_handler
def parse_args(args, user_id, manager):
    if args.which != 'list':
        return

    if args.add:
        manager.add_task_to_list(user_id, args.list_name, args.add[0])

    if args.remove:
        manager.remove_task_from_list(user_id, args.list_name, args.remove[0])

    if args.list:
        output.print_list_tasks(user_id, args.list_name, manager.storage)