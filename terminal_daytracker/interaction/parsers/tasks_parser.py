# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from terminal_daytracker.interaction import output
from lib.entity.task import Status, Priority
from lib import exceptions
from lib.logger import get_logger
from terminal_daytracker.interaction.parsers.exceptions_wrapper import library_exceptions_handler


def init_parser(subparsers):
    tasks_parser = subparsers.add_parser("tasks", 
        help='handling all user tasks, includes notifications')
    tasks_parser.add_argument('-f', '--finish', nargs=1,
                              help='finish task with some hash')
    tasks_parser.add_argument('-a', '--active', action='store_true',
                              help='list of current active tasks')
    tasks_parser.add_argument('-i', '--info', nargs=1, dest='task_info',
                              help='detailed description of the task')
    tasks_parser.add_argument('-n', '--notifications', choices=['low', 'normal', 'high'],
                              help='show current prioritized tasks')
    tasks_parser.add_argument('-l', '--list', action='store_true',
                              help='show all user tasks')
    tasks_parser.set_defaults(which='tasks')

    add_subparser = tasks_parser.add_subparsers(help='add new task to current user')
    task_add_parser = add_subparser.add_parser("add")
    task_add_parser.add_argument('date', 
        help='task date and time, format \'YYYY-MM-DD HH:MM\'',
        type=lambda t: datetime.strptime(t, '%Y-%m-%d %H:%M'))
    task_add_parser.add_argument('title', help='task title')
    task_add_parser.set_defaults(which='add task')


@library_exceptions_handler
def parse_args(args, user_id, manager):
    parse_tasks_args(args, user_id, manager)
    parse_add_task(args, user_id, manager)


def parse_add_task(args, user_id, manager):
    if args.which != 'add task':
        return

    task_id = manager.create_task(title=args.title, finish=args.date.strftime('%Y-%m-%d %H:%M'), 
                                    creator=user_id)
    manager.add_task(user_id, task_id)


def parse_tasks_args(args, user_id, manager):
    if args.which != 'tasks':
        return

    if args.finish:
        manager.mark_task_finished(user_id=user_id, task_id=args.finish[0])

    if args.active:
        tasks = manager.storage.get_filtered_tasks(user_reader=user_id, status=Status.ACTIVE)
        for task_id in tasks:
            output.print_task_info(task_id, manager.storage)

    if args.task_info:
        output.print_detailed_task_info(args.task_info[0], 
                    manager.storage, manager.get_task_tree(user_id=user_id, task_id=args.task_info[0]))

    if args.notifications:
        tasks = manager.storage.get_filtered_tasks(user_reader=user_id, status=Status.ACTIVE,
            priority=Priority[args.notifications.upper()])
        for task_id in tasks:
            time = manager.get_nearest_notification(task_id)
            print('{0}  -  {1}'.format(time, task_id))

    if args.list:
        tasks_list = manager.storage.get_filtered_tasks(user_reader=user_id)
        for task_id in tasks_list:
            output.print_task_info(task_id, manager.storage)

