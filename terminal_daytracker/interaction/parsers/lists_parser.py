# -*- coding: utf-8 -*-
import logging
from lib import exceptions
from lib.logger import get_logger
from lib import data_storage
from terminal_daytracker.interaction.parsers.exceptions_wrapper import library_exceptions_handler


def init_parser(subparsers):
    lists_parser = subparsers.add_parser("lists", description='user lists handler',
        help='operations with user lists')

    lists_parser.add_argument('-a', '--add', nargs=1, dest='add_list',
                              help='Add new user list')
    lists_parser.add_argument('-r', '--remove', nargs=1, dest='delete_list',
                              help='Remove user list')
    lists_parser.add_argument('-l', '--list', action='store_true',
                              help='List of user lists')

    lists_parser.set_defaults(which='lists')


@library_exceptions_handler
def parse_args(args, user_id, manager):
    if args.which != 'lists':
        return

    if args.add_list:
        manager.storage.update_user_list(user_id, args.add_list[0], set())

    if args.delete_list:
        manager.storage.update_user_list(user_id, args.delete_list[0])

    if not args.add_list and not args.delete_list:
        lists = manager.storage.get_user_lists_names(user_id)
        for name in lists:
            print(name)