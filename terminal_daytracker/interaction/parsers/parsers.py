# -*- coding: utf-8 -*-
from terminal_daytracker.interaction.parsers import tasks_parser
from terminal_daytracker.interaction.parsers import lists_parser
from terminal_daytracker.interaction.parsers import plans_parser
from terminal_daytracker.interaction.parsers import task_editor_parser
from terminal_daytracker.interaction.parsers import list_parser
from terminal_daytracker.interaction.parsers import user_parser


def init_parsers(subparsers):
    task_editor_parser.init_parser(subparsers)
    tasks_parser.init_parser(subparsers)
    lists_parser.init_parser(subparsers)
    plans_parser.init_parser(subparsers)
    list_parser.init_parser(subparsers)
    user_parser.init_parser(subparsers)


def parse_args(args, user_id, manager):
    if user_id is None:
        print('User not found. Please sign in.')
        return
    tasks_parser.parse_args(args, user_id, manager)
    lists_parser.parse_args(args, user_id, manager)
    plans_parser.parse_args(args, user_id, manager)
    task_editor_parser.parse_args(args, user_id, manager)
    list_parser.parse_args(args, user_id, manager)