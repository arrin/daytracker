# -*- coding: utf-8 -*-
import logging
from datetime import datetime
from lib import exceptions
from lib.logger import get_logger
from lib import data_storage
from lib.entity.task import Priority
from lib.tasks_manager import TasksManager
from terminal_daytracker.interaction.parsers.exceptions_wrapper import library_exceptions_handler


def init_parser(subparsers):
    tasks_parser = subparsers.add_parser("edit", help='task editing',
        description='edit tasks properties')

    tasks_parser.add_argument('task_id', help='identifier of the processed task')
    tasks_parser.add_argument('-t', '--title', nargs=1, help='set task title')
    tasks_parser.add_argument('-r', '--remove', action='store_true', help='remove task')
    tasks_parser.add_argument('-a', '--archive', action='store_true', help='archive task')
    tasks_parser.add_argument('-s', '--separate', action='store_true', 
        help='separate task from parent task and make it independent')
    tasks_parser.add_argument('-f', '--finish', nargs=1, 
        help='set task finish date, format \'YYYY-MM-DD HH:MM\'',
        type=lambda d:datetime.strptime(d, '%Y-%m-%d %H:%M'))
    tasks_parser.add_argument('-p', '--priority', choices=['low', 'normal', 'high'], 
        help='set task priority')
    tasks_parser.set_defaults(which='editor')

    add_subparser = tasks_parser.add_subparsers(
        help='allow to add notifications and subtasks to the task',
        description='allow to add notifications to the task and make it task depended of another task')
    task_add_parser = add_subparser.add_parser("add")
    task_add_parser.set_defaults(which='add')
    task_add_parser.add_argument('-n', '--notification', nargs=1, 
        help='add notification, format \'YYYY-MM-DD HH:MM\'',
        type=lambda d:datetime.strptime(d, '%Y-%m-%d %H:%M'))
    task_add_parser.add_argument('-s', '--subtask', nargs=1, 
        help='add subtask with some title, subtasks finish date will set similar as tasks')
    task_add_parser.add_argument('-i', '--info', nargs=1,
        help='add information relations between tasks')


@library_exceptions_handler
def parse_args(args, user_id, manager):
    parser_editing(args, user_id, manager)
    parse_adding(args, user_id, manager)
    

def parser_editing(args, user_id, manager):
    if args.which != 'editor':
        return

    if args.title:
        manager.set_task_field(user_id=user_id, task_id=args.task_id, title=args.title[0])
        
    if args.separate:
        manager.separate_tasks(user_id, args.task_id, args.separate[0])

    if args.finish:
        manager.set_task_field(user_id=user_id, task_id=args.task_id,
                                finish_time=args.finish[0].strftime('%Y-%m-%d %H:%M'))

    if args.priority:
        priority = Priority[args.priority.upper()]
        manager.set_task_field(user_id=user_id, task_id=args.task_id,
                                priority=priority)

    if args.remove:
        manager.remove_task(user_id, args.task_id)

    if args.archive:
        manager.mark_task_archived(user_id, args.task_id)

    if not any([args.title, args.archive, args.remove, 
                args.priority, args.finish, args.separate]):
        print('Task is exist.')


def parse_adding(args, user_id, manager):
    if args.which != 'add':
        return

    if args.notification:
        manager.add_task_notification(user_id, args.task_id,
            args.notification[0].strftime('%Y-%m-%d %H:%M'))

    if args.subtask:
        parent_task_id = args.task_id 
        manager.add_subtask(user_id, parent_task_id, args.subtask[0])

    if args.info:
        manager.add_information_relations(user_id, args.task_id, args.info[0])