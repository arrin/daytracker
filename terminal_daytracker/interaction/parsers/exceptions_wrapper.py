
from lib import exceptions


def library_exceptions_handler(func):
    """
        Decorator for handling library exceptions on parsing.
        
    """

    def args_parser(*args, **kwargs):
        try:
            return func(*args, **kwargs)     
        except Exception as e:
            print(str(e))

    return args_parser