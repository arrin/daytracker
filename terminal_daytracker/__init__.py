"""
    DayTracker console application.
    You can use ''day'' for work with it.
    More information on ReadMe.md

    Packages:
        interaction - provides functions for user interaction with terminal

    Modules:
        config_maker - provides working with config
        tracker - main application function

    For lib usage guide use ``help(lib)``
    
"""