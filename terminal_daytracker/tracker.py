#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import argparse
from lib import exceptions
from terminal_daytracker.config_maker import validate_config
from terminal_daytracker.interaction import user_loading
from terminal_daytracker.interaction.parsers import parsers
from terminal_daytracker.interaction.parsers.argument_parser import DayTrackerArgumentParser
from terminal_daytracker.interaction.logger_setup import init_logger
from terminal_daytracker.interaction.storage_setup import init_storage
from terminal_daytracker.interaction.parsers import user_parser
from lib.tasks_manager import TasksManager


def main():
    config_path = '~/.DayTracker/config.cfg'

    parser = DayTrackerArgumentParser(description='DayTracker')

    parser.set_defaults(which='main')
    subparsers = parser.add_subparsers()

    user_loading.check_config_dir(config_path)
    try:
        validate_config(config_path)
    except exceptions.InvalidConfig as e:
        print('{} Use config maker ``dayconfig``'.format(str(e)))
        return

    parsers.init_parsers(subparsers)
    init_logger(config_path)
    storage = init_storage(config_path)
    manager = TasksManager(storage)
    manager.update_tasks_status()
    manager.update_plans()

    args = parser.parse_args()
    user_id = user_parser.parse_args(args, config_path, manager)
    parsers.parse_args(args, user_id, manager)
    if args.which == 'main':
        parser.print_help()


if __name__ == "__main__":
    main()
