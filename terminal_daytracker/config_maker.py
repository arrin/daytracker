#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import argparse
import os
from lib.exceptions import InvalidConfig


class ConfigFieldsNames:
    NAME = 'APPDATA'
    PATH = 'path'
    USER = 'user'
    LOGGING = 'logging'
    CONSOLE_MODE = 'console'
    FILE_MODE = 'file'


def main():
    config_path = '~/.DayTracker'

    if not os.path.exists(os.path.expanduser(config_path)):
        os.mkdir(os.path.expanduser(config_path))

    parser = argparse.ArgumentParser(description='Config maker for DayTracker')
    parser.add_argument('-l', '--list', action='store_true',
                        help='Show config settings.')
    parser.add_argument('-d', '--default',
                        action='store_true', help='Set default config')
    parser.add_argument('-p', '--path', nargs=1, help='Path for userdata dir')
    parser.add_argument('-u', '--user', nargs=1, help='Set default user')
    parser.add_argument('-log', '--logger',
                        choices=[ConfigFieldsNames.CONSOLE_MODE, ConfigFieldsNames.FILE_MODE], 
                        help='Set logger type')

    args = parser.parse_args()
    config_file_path = os.path.join(os.path.expanduser(config_path), 'config.cfg')

    config = configparser.RawConfigParser()
    if args.default:
        config.add_section(ConfigFieldsNames.NAME)
        config[ConfigFieldsNames.NAME][ConfigFieldsNames.USER] = 'No user'
        config[ConfigFieldsNames.NAME][ConfigFieldsNames.PATH] = config_path
        config[ConfigFieldsNames.NAME][ConfigFieldsNames.LOGGING] = ConfigFieldsNames.CONSOLE_MODE
        with open(config_file_path, 'w') as f:
            config.write(f)

    elif not os.path.exists(config_file_path):
        print('ConfigFieldsNames not found.')
        
    elif args.list:
        with open(config_file_path, 'r') as f:
            for line in f.readlines():
                print(line)

    else:
        config.read(config_file_path)
        if args.logger:
            config[ConfigFieldsNames.NAME][ConfigFieldsNames.LOGGING] = args.logger

        if args.user:
            config[ConfigFieldsNames.NAME][ConfigFieldsNames.USER] = args.user[0]

        if args.path:
            config[ConfigFieldsNames.NAME][ConfigFieldsNames.PATH] = args.path[0]

        with open(config_file_path, 'w') as f:
            config.write(f)


def validate_config(config_path):
    config = configparser.RawConfigParser()
    config.read(os.path.expanduser(config_path))
    try:
        l = config[ConfigFieldsNames.NAME][ConfigFieldsNames.LOGGING]
        if l != ConfigFieldsNames.CONSOLE_MODE and l != ConfigFieldsNames.FILE_MODE:
            raise InvalidConfig()
        config[ConfigFieldsNames.NAME][ConfigFieldsNames.USER]
        config[ConfigFieldsNames.NAME][ConfigFieldsNames.PATH]
    except Exception:
        raise InvalidConfig()


if __name__ == "__main__":
    main()
