import unittest
import os
import shutil
from lib.logger import disable_logger
from lib import data_storage
from lib.entity.task import Status, Permissions
from lib.entity.user_lists import UserLists
from lib import exceptions
from lib.tasks_manager import TasksManager
from lib.storage.json_storage import JSONStorage


class UserLogicTest(unittest.TestCase):
    def setUp(self):
        disable_logger()
        self.test_dir = os.path.join(os.path.abspath(os.curdir), 'test_dir')
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.mkdir(self.test_dir)
        storage = data_storage.Storage(JSONStorage(self.test_dir))
        self.tasks_manager = TasksManager(storage)   
        self.user = 'tester'

    def test_user_finish_task(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.mark_task_finished(self.user, task_id)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.status == Status.FINISHED)

    def test_user_finish_task_without_permissions(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        self.tasks_manager.add_task(self.user, task_id, Permissions.READ)
        try:
            self.tasks_manager.mark_task_finished(self.user, task_id)
        except exceptions.PermissionDenied:
            pass
        except Exception:
            raise
        task = self.tasks_manager.get_task(task_id)
        self.assertFalse(task.status == Status.FINISHED)

    def test_finish_tasks_withour_subtasks(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        second_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00')
        self.tasks_manager.set_subtask(task_id, second_task_id)
        self.tasks_manager.add_task(self.user, task_id)
        try:
            self.tasks_manager.mark_task_finished(self.user, task_id)
        except exceptions.ImpossibleToFinish:
            pass
        except Exception:
            raise

    def test_finish_tasks_with_subtasks(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        second_task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        self.tasks_manager.set_subtask(task_id, second_task_id)
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.add_task(self.user, second_task_id)
        self.tasks_manager.mark_task_finished(self.user, second_task_id)
        self.tasks_manager.mark_task_finished(self.user, task_id)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.status == Status.FINISHED)

    def test_user_lists_adding(self):
        list_name = 'aaa'
        self.tasks_manager.storage.init_user_lists(user_id=self.user, lists=UserLists())
        self.tasks_manager.storage.update_user_list(user_id=self.user, list_name=list_name, tasks=set())
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.add_task_to_list(self.user, list_name, task_id)
        user_list = self.tasks_manager.get_user_list(self.user, list_name)
        self.assertTrue(len(user_list) == 1)
        self.assertTrue(task_id in user_list)

    def test_user_lists_removing(self):
        list_name = 'aaa'
        self.tasks_manager.storage.init_user_lists(user_id=self.user, lists=UserLists())
        self.tasks_manager.storage.update_user_list(user_id=self.user, list_name=list_name, tasks=set())
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.add_task_to_list(self.user, list_name, task_id)
        self.tasks_manager.remove_task_from_list(self.user, list_name, task_id)
        user_list = self.tasks_manager.get_user_list(self.user, list_name)
        self.assertTrue(len(user_list) == 0)
        self.assertFalse(task_id in user_list)

    def test_add_task_to_list_without_access(self):
        list_name = 'aaa'
        self.tasks_manager.storage.init_user_lists(user_id=self.user, lists=UserLists())
        self.tasks_manager.storage.update_user_list(user_id=self.user, list_name=list_name, tasks=set())
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        try:
            self.tasks_manager.add_task_to_list(user_id=self.user, list_name=list_name, task_id=task_id)
        except exceptions.PermissionDenied:
            pass
        except Exception:
            raise
        user_list = self.tasks_manager.get_user_list(self.user, list_name)
        self.assertTrue(len(user_list) == 0)
        self.assertFalse(task_id in user_list)

    def test_remove_non_existent_task(self):
        list_name = 'aaa'
        self.tasks_manager.storage.init_user_lists(user_id=self.user, lists=UserLists())
        self.tasks_manager.storage.update_user_list(user_id=self.user, list_name=list_name, tasks=set())
        try:
            self.tasks_manager.remove_task_from_list(user_id=self.user, list_name=list_name, task_id='123')
        except exceptions.TaskNotFound:
            pass
        except Exception:
            raise
        user_list = self.tasks_manager.get_user_list(self.user, list_name)
        self.assertTrue(len(user_list) == 0)

    def test_adding_to_non_existent_list(self):
        list_name = 'aaa'
        self.tasks_manager.storage.init_user_lists(user_id=self.user, lists=UserLists())
        self.tasks_manager.storage.update_user_list(user_id=self.user, list_name=list_name, tasks=set())
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        self.tasks_manager.add_task(self.user, task_id)
        try:
            self.tasks_manager.add_task_to_list(user_id=self.user, list_name='111', task_id=task_id)
        except exceptions.ListNotFound:
            pass
        except Exception:
            raise
        user_lists = self.tasks_manager.storage.get_user_lists_names(self.user)
        self.assertTrue(len(user_lists) == 1)

    def test_remove_task(self):
        self.tasks_manager.storage.init_user_lists(user_id=self.user, lists=UserLists())
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00')
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.remove_task(self.user, task_id)


    def tearDown(self):
        shutil.rmtree(self.test_dir)


if __name__ == "__main__":
    unittest.main()