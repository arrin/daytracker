import unittest
import os
import shutil
from lib.logger import disable_logger
from lib import exceptions
from lib.entity.task import (Task,
                             Permissions)
from lib.entity.plan import Plan
from lib.data_storage import Storage
from lib.storage.json_storage import JSONStorage


class StorageTests(unittest.TestCase):
    def setUp(self):
        disable_logger()
        self.test_dir = os.path.join(os.path.abspath(os.curdir), 'test_dir')
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.mkdir(self.test_dir)
        self.storage = Storage(JSONStorage(self.test_dir))
        self.user = 'tester'


    def test_plans_storage(self):
        plan = Plan(id='12345')
        self.storage.update_plan(plan.id, plan)
        new_plan = self.storage.get_plan(plan.id)
        self.assertTrue(plan.id == new_plan.id)


    def test_tasks_matching(self):
        title = 'task'
        task = Task(title=title)
        self.storage.update_task(task.id, task)
        task = Task(title=title)
        self.storage.update_task(task.id, task)
        task = Task(title=title)
        self.storage.update_task(task.id, task)
        task = Task(title='ksat')
        self.storage.update_task(task.id, task)

        self.assertTrue(len(self.storage.get_filtered_tasks(title=title)) == 3)
        self.assertTrue(len(self.storage.get_filtered_tasks(title='ksat')) == 1)


    def test_task_fields(self):
        task = Task()
        task_id = task.id
        self.storage.update_task(task_id=task_id, task=task)
        history = ['1', '1']
        self.storage.update_task_field(task_id=task_id, history=history)
        self.assertEqual(history, self.storage.get_task_field(task_id=task_id, history=True))


    def test_tasks_user_matching(self):
        title = 'task'
        task = Task(title=title, users={self.user: Permissions.ADMIN.value})
        self.storage.update_task(task.id, task)
        task = Task(title=title)
        self.storage.update_task(task.id, task)
        task = Task(title=title)
        self.storage.update_task(task.id, task)
        task = Task(title='ksat')
        self.storage.update_task(task.id, task)
        
        self.assertTrue(len(self.storage.get_filtered_tasks(user_reader=self.user, title=title)) == 1)
        self.assertFalse(len(self.storage.get_filtered_tasks(user_reader='another', title=title)))


    def test_tasks_matching_date(self):
        first_finish = '2018-06-22 12:00'
        first_task = Task(finish=first_finish)
        second_finish = '2018-05-31 12:00'
        second_task = Task(finish=second_finish)
        self.storage.update_task(first_task.id, first_task)
        self.storage.update_task(second_task.id, second_task)

        first_day = '2018-06-22'
        self.assertTrue(len(self.storage.get_filtered_tasks(finish_date=first_day)) == 1)
        another_day = '2018-12-12'
        self.assertTrue(len(self.storage.get_filtered_tasks(finish_date=another_day)) == 0)

        new_finish = '2018-06-22 23:00'
        new_task = Task(finish=new_finish)
        self.storage.update_task(new_task.id, new_task)
        self.assertTrue(len(self.storage.get_filtered_tasks(finish_date=first_day)) == 2)


    def tearDown(self):
        self.storage.clear()
        shutil.rmtree(self.test_dir)


if __name__ == "__main__":
    unittest.main()