import unittest
import os
import shutil
from datetime import timedelta
from lib.logger import disable_logger
from lib.entity.task import Task, Status, Permissions
from lib.storage.json_storage import JSONStorage
from lib.data_storage import Storage
from lib import exceptions
from lib.tasks_manager import TasksManager


class PlansTests(unittest.TestCase):
    def setUp(self):
        disable_logger()
        self.test_dir = os.path.join(os.path.abspath(os.curdir), 'test_dir')
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.mkdir(self.test_dir)
        storage = Storage(JSONStorage(self.test_dir))
        self.tasks_manager = TasksManager(storage)

    def test_create_plan(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', starting_date='2018-12-12 12:00',
                                                 time_delta=timedelta(days=1))
        plan = self.tasks_manager.storage.get_plan(plan_id)
        self.assertTrue(plan.title == 'test')
        self.assertTrue(plan.user_id == '0')
        self.assertTrue(plan.last_added == '2018-12-12 12:00')
        self.assertTrue(len(self.tasks_manager.storage.get_filtered_tasks()) == 1)

    def test_add_next_task(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', starting_date='2018-12-12 12:00',
                                                 time_delta=timedelta(days=1))
        self.tasks_manager.add_plan_task(plan_id=plan_id, next_date='2018-12-12 15:00')
        plan = self.tasks_manager.storage.get_plan(plan_id)
        self.assertTrue(plan.title == 'test')
        self.assertTrue(plan.user_id == '0')
        self.assertTrue(plan.last_added == '2018-12-12 15:00')
        self.assertTrue(len(self.tasks_manager.storage.get_filtered_tasks()) == 2)
        self.assertTrue(plan.last_added_id  != None)

    def test_remove_plan(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', starting_date='2018-12-12 12:00',
                                                 time_delta=timedelta(days=1))
        self.tasks_manager.storage.update_plan(plan_id)
        try:
            self.tasks_manager.storage.get_plan(plan_id)
        except exceptions.PlanNotFound:
            pass
        except Exception:
            raise

    def test_add_next_task_weekdays(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', 
            starting_date='2040-06-09 01:00', weekdays='0010010')
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2040-06-13 01:00')

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2040-06-16 01:00')


    def test_add_next_task_weekdays_with_repetitions(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', 
            starting_date='2040-06-09 01:00', weekdays='0010010', repeat=2)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2040-06-13 01:00')

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2040-06-13 01:00')
        self.assertTrue(len(self.tasks_manager.storage.get_filtered_tasks()) == 2)


    def test_add_next_task_weekdays_with_extreme_date(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', starting_date='2040-06-09 01:00', 
            weekdays='0010010', extreme_date='2040-06-14 01:00')
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2040-06-13 01:00')

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2040-06-13 01:00')


    def test_add_next_task_timedelta(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', 
            starting_date='3000-12-12 01:00', time_delta=timedelta(hours=2))
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '3000-12-12 03:00')


    def test_add_next_task_timedelta_2(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', 
            starting_date='3000-12-12 01:00', time_delta=timedelta(days=1, hours=12))
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id

        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '3000-12-13 13:00')

    def test_add_next_task_timedelta_3(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', 
            starting_date='2017-06-09 01:00', time_delta=timedelta(days=365))
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id

        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2019-06-09 01:00')
        self.tasks_manager.set_task_properties(task_id, status=Status.FINISHED)
        self.tasks_manager.update_plan_task(plan_id)
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        task = self.tasks_manager.storage.get_task(task_id)
        self.assertTrue(task.finish == '2020-06-08 01:00')


    def test_add_next_task_timedelta_with_extreme_date(self):
        plan_id = self.tasks_manager.create_plan(user_id='0', title='test', starting_date='2017-06-09 01:00', 
                time_delta=timedelta(days=365), extreme_date='2018-06-09 01:00')
        task_id = self.tasks_manager.storage.get_plan(plan_id).last_added_id
        self.assertEqual(task_id, None)


    def tearDown(self):
        self.tasks_manager.storage.clear()
        shutil.rmtree(self.test_dir)


if __name__ == "__main__":
    unittest.main()

