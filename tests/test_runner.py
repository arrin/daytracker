#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from tests.plans_logic_test import PlansTests
from tests.storage_test import StorageTests
from tests.task_logic_test import TaskLogicTests
from tests.user_lists_logic_test import UserLogicTest


def get_test_suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PlansTests))
    suite.addTests(unittest.makeSuite(StorageTests))
    suite.addTests(unittest.makeSuite(TaskLogicTests))
    suite.addTests(unittest.makeSuite(UserLogicTest))
    return suite
    