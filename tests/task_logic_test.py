import unittest
import os
import shutil
from lib.logger import disable_logger
from lib import data_storage
from lib.storage.json_storage import JSONStorage
from lib import exceptions
from lib.tasks_manager import TasksManager
from lib.entity.task import Status


class TaskLogicTests(unittest.TestCase):
    def setUp(self):
        disable_logger()
        self.test_dir = os.path.join(os.path.abspath(os.curdir), 'test_dir')
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        os.mkdir(self.test_dir)
        self.storage = data_storage.Storage(JSONStorage(self.test_dir))
        self.tasks_manager = TasksManager(self.storage)
        self.user = 'tester'

    def test_base_task_creating(self):
        finish_time = '2018-12-12 12:00'
        task_id = self.tasks_manager.create_task(title='test', finish=finish_time, creator=self.user)
        task = self.tasks_manager.get_task(task_id)

        self.assertTrue(task.title == 'test')
        self.assertTrue(task.finish == finish_time)
        self.assertTrue(finish_time in self.storage.get_task_field(task_id, notifications=True))

    def test_adding_task_notifications(self):
        time = '2018-12-12 15:00'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.add_notification(task_id, time)

        self.assertTrue(len(self.storage.get_task_field(task_id, notifications=True)) == 2)
        self.assertTrue(time in self.storage.get_task_field(task_id, notifications=True))

    def test_changing_task_fields(self):
        new_title = 'new'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.set_task_properties(task_id=task_id, title=new_title)
        task = self.tasks_manager.get_task(task_id)
        self.assertEqual(task.title, new_title)

        new_status = Status.ARCHIVED
        self.tasks_manager.set_task_properties(task_id=task_id, status=new_status)
        task = self.tasks_manager.get_task(task_id)
        self.assertEqual(task.status, new_status)

    def test_adding_exist_notification(self):
        time = '2018-12-12 12:00'
        task_id = self.tasks_manager.create_task(title='test', finish=time, creator=self.user)
        self.tasks_manager.add_notification(task_id, time)

        self.assertTrue(len(self.storage.get_task_field(task_id, notifications=True)) == 1)
        self.assertTrue(time in self.storage.get_task_field(task_id, notifications=True))

    def test_remove_notification(self):
        time = '2018-12-12 12:00'
        task_id = self.tasks_manager.create_task(title='test', finish=time, creator=self.user)
        self.tasks_manager.remove_notification(task_id, time)

        self.assertFalse(len(self.storage.get_task_field(task_id, notifications=True)))
        self.assertFalse(time in self.storage.get_task_field(task_id, notifications=True))

    def test_remove_not_existent_notification(self):
        time = '2018-12-12 15:00'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.remove_notification(task_id, time)

        self.assertTrue(len(self.storage.get_task_field(task_id, notifications=True)) == 1)
        self.assertFalse(time in self.storage.get_task_field(task_id, notifications=True))

    def test_get_nearest_notification(self):
        time = '2018-12-12 15:00'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 20:00', creator=self.user)
        self.tasks_manager.add_notification(task_id, time)
        note_time = self.tasks_manager.get_nearest_notification(task_id)
        self.assertTrue(note_time == time)

    def test_update_task_finish_time(self):
        time = '2018-12-12 15:00'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.update_finish_time(task_id, time)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.finish == time)
        self.assertTrue(len(self.storage.get_task_field(task_id, notifications=True)) == 1)

    def test_update_same_finish_time(self):
        time = '2018-12-12 12:00'
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.update_finish_time(task_id, time)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.finish == time)
        self.assertTrue(len(self.storage.get_task_field(task_id, notifications=True)) == 1)

    def test_set_subtask(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00')
        self.tasks_manager.set_subtask(task_id, another_task_id)
        task = self.tasks_manager.get_task(another_task_id)
        self.assertTrue(task.parent_task == task_id)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.parent_task is None)

    def test_try_cycle_subtask(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00')
        self.tasks_manager.set_subtask(task_id, another_task_id)
        try:
            self.tasks_manager.set_subtask(another_task_id, task_id)
        except exceptions.IncorrectRelations:
            pass
        except Exception:
            raise

    def test_split_subtask(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00')
        self.tasks_manager.set_subtask(task_id, another_task_id)
        self.tasks_manager.add_task(user_id=self.user, task_id=another_task_id)
        self.tasks_manager.split_tasks(self.user, task_id, another_task_id)

        task = self.tasks_manager.get_task(another_task_id)
        self.assertTrue(task.parent_task is None)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.parent_task is None)

    def test_split_incorrect_task(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.add_task(user_id=self.user, task_id=another_task_id)
        try:
            self.tasks_manager.split_tasks(self.user, task_id, another_task_id)
        except exceptions.IncorrectRelations:
            pass
        except Exception:
            raise
        task = self.tasks_manager.get_task(another_task_id)
        self.assertTrue(task.parent_task is None)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(task.parent_task is None)

    def test_get_root_task(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        third_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00')
        self.tasks_manager.set_subtask(task_id, another_task_id)
        self.tasks_manager.set_subtask(another_task_id, third_task_id)

        self.assertTrue(self.tasks_manager.get_root_task(task_id) 
                            == self.tasks_manager.get_root_task(task_id))
        self.assertTrue(self.tasks_manager.get_root_task(another_task_id) 
                            == self.tasks_manager.get_root_task(third_task_id))
        self.assertTrue(self.tasks_manager.get_root_task(task_id) 
                            == self.tasks_manager.get_root_task(third_task_id))

    def test_remove_task(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.add_task(user_id=self.user, task_id=task_id)
        self.tasks_manager.remove_task(user_id=self.user, task_id=task_id)
        new_task_id = None
        try:
            self.tasks_manager.add_notification(task_id, time='12:00')
            new_task_id = self.tasks_manager.get_root_task(task_id)
        except exceptions.TaskNotFound:
            pass
        except Exception:
            raise
        self.assertTrue(new_task_id is None)

    def test_remove_root_task(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        another_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        third_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.set_subtask(task_id, another_task_id)
        self.tasks_manager.set_subtask(task_id, third_task_id)
        
        self.tasks_manager.add_task(user_id=self.user, task_id=task_id)
        self.tasks_manager.remove_task(user_id=self.user, task_id=task_id)
        task = self.tasks_manager.get_task(another_task_id)
        self.assertTrue(task.parent_task is None)
        task = self.tasks_manager.get_task(third_task_id)
        self.assertTrue(task.parent_task is None)

    def test_remove_not_exist_task(self):
        try:
            self.tasks_manager.remove_task(user_id=self.user, task_id='12345')
        except exceptions.TaskNotFound:
            pass
        except Exception:
            raise

    def test_remove_outdated_notifications(self):
        task_id = self.tasks_manager.create_task(title='test', finish='1990-12-12 12:00', creator=self.user)
        time = '3000-12-12 12:00'
        self.tasks_manager.add_notification(task_id, time)
        count = self.tasks_manager.remove_outdated_notifications(task_id)
        self.assertTrue(count == 1)
        task = self.tasks_manager.get_task(task_id)
        self.assertTrue(len(task.notifications) == 1)
        self.assertTrue(self.tasks_manager.get_nearest_notification(task_id) == time)

    def test_add_information_relations(self):
        task_id = self.tasks_manager.create_task(title='test', finish='2018-12-12 12:00', creator=self.user)
        second_task_id = self.tasks_manager.create_task(title='subtest', finish='2018-12-12 12:00', creator=self.user)
        self.tasks_manager.add_task(self.user, task_id)
        self.tasks_manager.add_task(self.user, second_task_id)
        self.tasks_manager.add_information_relations(self.user, task_id, second_task_id)
        self.assertEqual(len(self.storage.get_task_field(task_id=task_id, informations=True)), 1)
        self.assertEqual(self.storage.get_task_field(task_id, informations=True)[0], second_task_id)


    def tearDown(self):
        self.tasks_manager.storage.clear()
        shutil.rmtree(self.test_dir)


if __name__ == "__main__":
    unittest.main()