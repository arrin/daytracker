# DayTracker

## Overview
DayTracker is a tool to manage your time. It allows users to create lists to manage their tasks, add due dates, reminders, subtasks, tasks that repeating with some plan, for example, every monday. Also it allows to work together by creating task for several users or giving some subtask for other users to speed up your work.

## Requirements
For correct usage you need *Python 3.5* at least.
Clone repository.
```bash
git clone git@bitbucket.org:arrin/daytracker.git
```

The project requires a handful of python packages. Install them using:
```bash
pip3 install -r requirements.txt
```

## Terminal version usage
Install application.
```bash
python3 setup.py install
```
Before you start using application u must configure it. To set up default configuration you can use:
```bash
dayconfig -d
```
You can customize path for data storing or logging type with ***dayconfig*** too. For more information add ***-h***.


To start usage enter to sign up as new user
```bash
day user -s 'tester'
```
For task handling and navigation daytracker uses task ids, unique 5-char strings, you can get access, edit or remove task by its id.

Usage examples:
Task creating, task got id 15707:
```bash
day tasks add '2018-07-04 12:00' just_another_day
created task 15707
user tester get access to task 15707
```

Editing previous task finish date:
```bash
day edit 15707 -f '2018-07-02 14:00'
user tester set task 15707 finish time to 2018-07-02 14:00
```

Task detailed information:
```bash
day tasks -i 15707
15707	    just_another_day  (2018-07-02 14:00)  normal priority
Story:
	2018-06-13 17:34  -  user tester added information relations between 15707 and 94398
	2018-06-13 17:34  -  user tester add subtask 06617 to task 15707
	2018-06-13 17:23  -  user tester set task 15707 finish time to 2018-07-02 14:00
	2018-06-13 17:18  -  user tester get access to task 15707
	2018-06-13 17:18  -  created task 15707
Subtasks:
	06617	          cup_of_tea  (2018-07-13 08:00)  normal priority
Information relations:
	94398	interesting_task_may  (2018-06-28 09:00)  high priority

```

## Library usage
You can use application library without terminal interaction. Library has some functions, are not allowed on terminal version.
####How to use?
-  import dependencies
```python
from lib.storage.json_storage import JSONStorage
from lib.storage.data_storage import Storage
from lib.tasks_manager import TasksManager
```

-  Create json-storage object form lib.storage.json_storage.JSONStorage with your storage path.
```python
json_storage_implementatiton = JSONStorage(path)
```
-  Create Storage interface instance with JSONStorage implementation.  
```python
storage = Storage(json_storage_implementatiton)
``` 
-  Create task_manager instance with *storage* implementation.
```python
manager = TasksManager(storage)
```

- You can use *manager* functions to handle tasks, user permissions, plans, user lists, etc.
- For more documentation use *help()*.

Usage example
```python
>>> from lib.storage.json_storage import JSONStorage
>>> from lib.storage.data_storage import Storage
>>> from lib.tasks_manager import TasksManager
>>> path = '~/.DayTracker'
>>> storage_impl = JSONStorage(path)
>>> storage = Storage(storage_impl)
>>> manager = TasksManager(storage)
>>> new_task_id = manager.create_task('another_task', '2018-06-16 12:00')
>>> print(new_task_id)
a3fe4
```
- If you modify library or storage implementation, you can test it.
```bash
python3 setup.py test
```

##Web version deploying
Developed using Django framework.
Web version uses SQLite database and default Django server.
Clone repository.
```bash
git clone git@bitbucket.org:arrin/daytracker.git
```

The project requires a handful of python packages. Install them using:
```bash
pip3 install -r requirements.txt
```

Make migrations:
```bash
python3 manage.py makemigration
python3 manage.py migrate
```

To run server enter:
```bash
python3 manage.py runserver 127.0.0.1:8000
```


## Author's note
Good luck in your business and do not forget to wisely manage your time!
