"""
    Core library module.

    Classes:
        TasksManager - core logic class, provides functions for task, plans, user_lists managment.

    Usage examples:
        Task manager creation::
            >>> from lib.tasks_manager import TasksManager
            >>> manager = TasksManager(data_storage=storage_implementation)

        Task creation::
            >>> task_id = manager.create_task(title='test task', finish='2018-12-12 12:00')

        Adding user permissions to task::
            >>> manager.add_task(user_id=username, task_id=task_id)

        Working with notifications::
            >>> time = '2018-12-12 12:00'
            >>> manager.add_notification(task_id=task_id, time=time)
            >>> nearest_notification = manager.get_nearest_notification(task_id=task_id)
            >>> manager.remove_notification(task_id=task_id, time=time)
            >>> manager.remove_outdated_notifications(task_id=task_id)

        Access to manager storage::
            >>> plans_ids = manager.storage.get_plans(user_id=username)

"""

from collections import namedtuple
from datetime import datetime, timedelta
from uuid import uuid4
from lib import exceptions
from lib.logger import get_logger
from lib.entity.plan import Plan
from lib.data_storage import Storage
from lib.entity.task import (Task,
                             Status,
                             Permissions,
                             OperationType,
                             TaskActionRecord)


class TasksManager:
    """
        Core logic class.

        Args:
            storage: storage implementation for objects storing, lib.storage.data_storage.Storage object

    """

    def __init__(self, data_storage, gen_id=lambda: str(uuid4().hex)[-5:]):
        """
            Args:
                data_storage: storage implementation, lib.storage.data_storage.Storage instance
                gen_id: method for unique id generation

        """

        if not isinstance(data_storage, Storage):
            raise TypeError('data_storage must be instance of Storage class')

        self.storage = data_storage
        self.gen_id = gen_id

    # region user_tasks_interaction
    def mark_task_finished(self, user_id, task_id):
        """
            Check user if user has access to finish task and finish it.

            Args:
                user_id: user id
                task_id: handling task id

            Raises:
                PermissionDenied exception

        """

        if not self.validate_permissions(user_id, task_id, OperationType.FINISH):
            raise exceptions.PermissionDenied(user_id, task_id)

        if self.storage.get_filtered_tasks(status=Status.ACTIVE, parent_task=task_id):
            raise exceptions.ImpossibleToFinish(task_id, 'Subtasks not finished.')

        if self.storage.get_task(task_id).status == Status.FINISHED:
            get_logger().info('task {0} is already finished'.format(task_id))
            return

        self.set_task_properties(task_id=task_id, status=Status.FINISHED)

        message = 'user {0} finished task {1}'.format(user_id, task_id)
        get_logger().info(message)
        self.log_task_action(task_id, message)


    def add_task_notification(self, user_id, task_id, notification):
        """
            Check user if user has access to add notifications to the task and add it.

            Args:
                user_id: user id
                task_id: handling task id

            Raises:
                PermissionDenied exception
                
        """

        if not self.validate_permissions(user_id, task_id, OperationType.EDIT):
            raise exceptions.PermissionDenied(user_id, task_id)

        self.add_notification(task_id, notification)


    def set_task_field(self, user_id, task_id, finish_time=None, title=None, priority=None, status=None):
        """
            Check user if user has access to set task priority time and set it.

            Args:
                user_id: user id
                task_id: handling task id

            Raises:
                PermissionDenied exception
        """

        if not self.validate_permissions(user_id, task_id, OperationType.EDIT):
            raise exceptions.PermissionDenied(user_id, task_id)
        if finish_time is not None:
            self.update_finish_time(task_id, finish_time)
            message = 'user {0} set task {1} finish time to {2}'.format(user_id, task_id, finish_time)
            get_logger().info(message)
            self.log_task_action(task_id, message)
        if title is not None:
            message = 'user {0} set task {1} finish time to {2}'.format(user_id, task_id, title)
            get_logger().info(message)
            self.log_task_action(task_id, message)
        if priority is not None:
            message = 'user {0} set task {1} priority to {2}'.format(user_id, task_id, priority)
            get_logger().info(message)
            self.log_task_action(task_id, message)
        if status is not None:
            message = 'user {0} set task {1} status to {2}'.format(user_id, task_id, status)
            get_logger().info(message)
            self.log_task_action(task_id, message)
        self.set_task_properties(task_id=task_id, title=title, finish=finish_time, priority=priority, status=status)


    def split_tasks(self, user_id, parent_task_id, task_id):
        """
            Check user if user has access to split tasks and split it.

            Args:
                user_id: user id
                parent_task_id: handling parent task id
                task_id: handling task id

            Raises:
                PermissionDenied exception
                Incorrect Relations exception, if task don't have relations
        """

        if not self.validate_permissions(user_id, task_id, OperationType.EDIT):
            raise exceptions.PermissionDenied(user_id, task_id)

        task = self.storage.get_task(task_id)
        if task.parent_task != parent_task_id:
            raise exceptions.IncorrectRelations(parent_task_id, task_id)
        task.parent_task = None
        self.storage.update_task(task_id, task)
        message = ('user {0} splitted task {2} from task {1}'
                            .format(user_id, parent_task_id, task_id))
        get_logger().info(message)
        self.log_task_action(task_id, message)
        self.log_task_action(parent_task_id, message)


    def add_subtask(self, user_id, parent_task_id, task_id):
        """
            Check user if user has access to set atsk as subtask and set it.

            Args:
                user_id: user id
                parent_task_id: handling parent task id
                task_id: handling task id

            Raises:
                PermissionDenied exception
        """

        if not self.validate_permissions(user_id, task_id, OperationType.EDIT):
            raise exceptions.PermissionDenied(user_id, task_id)

        self.set_subtask(parent_task_id, task_id)

        message = ('user {0} add subtask {2} to task {1}'
                            .format(user_id, parent_task_id, task_id))
        get_logger().info(message)
        self.log_task_action(task_id, message)
        self.log_task_action(parent_task_id, message)


    def add_information_relations(self, user_id, task_id, second_task_id):
        """
            Add information relations between tasks.

            Args:
                user_id: user id
                task_id: handling task_id
                second_task_id: another task for relations

        """

        informations = self.storage.get_task_field(task_id, informations=True)
        if not second_task_id in informations:
            informations.append(second_task_id)
        self.storage.update_task_field(task_id, informations=informations)

        informations = self.storage.get_task_field(second_task_id, informations=True)
        if not task_id in informations:
            informations.append(task_id)
        self.storage.update_task_field(second_task_id, informations=informations)

        message = ('user {0} added information relations between {1} and {2}'
                    .format(user_id, task_id, second_task_id))
        get_logger().info(message)
        self.log_task_action(task_id, message)
        self.log_task_action(second_task_id, message)


    def remove_information_relations(self, user_id, task_id, second_task_id):
        """
            Remove task information relations.

            Args:
                user_id: user id
                task_id: handling task
                second_task_id: handling task id

        """

        informations = self.storage.get_task_field(task_id, informations=True)
        if second_task_id in informations:
            informations.remove(second_task_id)
        self.storage.update_task_field(task_id, informations=informations)

        informations = self.storage.get_task_field(second_task_id, informations=True)
        if task_id in informations:
            informations.remove(task_id)
        self.storage.update_task_field(second_task_id, informations=informations)

        message = ('user {0} removed information relations between {1} and {2}'
                    .format(user_id, task_id, second_task_id))
        get_logger().info(message)
        self.log_task_action(task_id, message)
        self.log_task_action(second_task_id, message)


    def remove_task(self, user_id, task_id):
        """
            Check user if user has access to remove task from system and remove it.

            Args:
                user_id: user id
                task_id: handling task id

            Raises:
                PermissionDenied exception

        """

        if not self.validate_permissions(user_id, task_id, OperationType.REMOVE):
            raise exceptions.PermissionDenied(user_id, task_id)

        try:
            user_lists = self.storage.get_user_lists_names(user_id)
            for list in user_lists:
                    self.remove_task_from_list(user_id, list, task_id)
        except exceptions.UserNotFound:
            pass
        message = 'user {0} removed task {1}'.format(user_id, task_id)
        self.log_task_action(task_id, message)
        self.storage.update_task(task_id)
        get_logger().info(message)

    def mark_task_archived(self, user_id, task_id):
        """
            Mark task archived and make it unactive for all users.

            Args:
                user_id: user id
                task_id: handling task id

            Raises:
                Permission Denied exception, if user can't handle task
                TaskNotFound exception

        """

        if not self.validate_permissions(user_id, task_id, OperationType.EDIT):
            raise exceptions.PermissionDenied(user_id, task_id)

        task = self.storage.get_task(task_id)
        task.status = Status.ARCHIVED

        message = 'user {0} archived task {1}'.format(user_id, task_id)
        self.log_task_action(task_id, message)
        self.storage.update_task(task_id, task)
        get_logger().info(message)


    def get_task_tree(self, user_id, task_id):
        """
            Get subtasks tree.

            Args:
                user_id: user requester
                task_id: root task id

            Returns:
                list (depth, task_id) in order

        """
        Vertex = namedtuple('Vertex', ['depth', 'task_id'])
        stack = [Vertex(depth=0, task_id=task_id)]
        task_tree = list()
        while stack:
            cur = stack.pop()
            task_tree.append(cur)
            for subtask_id in self.storage.get_filtered_tasks(user_reader=user_id, parent_task=cur.task_id):
                stack.append(Vertex(depth=cur.depth+1, task_id=subtask_id))
        return task_tree

    # endregion user_tasks_interaction

    # region user_lists
    def get_user_list(self, user_id, list_name):
        """
            Get user list.

            Args:
                user_id: user unique id
                list_name: users list name

        """

        return self.storage.get_user_list(user_id, list_name)


    def add_task_to_list(self, user_id, list_name, task_id):
        """
            Check user if user has access to read task and add it to user list.

            Args:
                user_id: user id
                list_name: user list name
                task_id: handling task id

            Raises:
                PermissionDenied exception
        """

        if not self.validate_permissions(user_id, task_id, OperationType.READ):
            raise exceptions.PermissionDenied(user_id, task_id)

        tasks_list = self.storage.get_user_list(user_id, list_name)
        tasks_list.append(task_id)
        self.storage.update_user_list(user_id, list_name, tasks_list)
        get_logger().info('user {0} add task {1} to list {2}'
                            .format(user_id, task_id, list_name))


    def remove_task_from_list(self, user_id, list_name, task_id):
        """
            Check user if user has access to read task and remove it from user list.

            Args:
                user_id: user id
                list_name: user list name
                task_id: handling task id

            Raises:
                PermissionDenied exception
        """

        if not self.validate_permissions(user_id, task_id, OperationType.READ):
            raise exceptions.PermissionDenied(user_id, task_id)

        tasks_list = self.storage.get_user_list(user_id, list_name)
        if task_id in tasks_list:
            tasks_list.remove(task_id)
        self.storage.update_user_list(user_id, list_name, tasks_list)
        get_logger().info('user {0} removed task {1} from list {2}'
                            .format(user_id, task_id, list_name))
    # endregion user_lists

    # region tasks_logic
    def create_task(self, title, finish, creator=None):
        """
            Create new task and upload it to the storage.

            Args:
                title: task title
                finish: finish date

        """

        date = datetime.now().strftime('%Y-%m-%d %H:%M')
        task = Task(title=title, finish=finish, created=date, id=self.gen_id(),
                     user_creator=creator)
        self.storage.update_task(task.id, task)
        self.add_notification(task.id, finish)
        message = 'created task {}'.format(task.id)
        get_logger().info(message)
        self.log_task_action(task.id, message)
        return task.id


    def add_notification(self, task_id, time):
        """
            Add new notification to the task.

            Args:
                task_id: handling task id
                time: notification time, format %Y-%m-%d %H:%M

        """

        notifications = self.storage.get_task_field(task_id, notifications=True)
        if not time in notifications:
            notifications.append(time)
        self.storage.update_task_field(task_id, notifications=notifications)


    def remove_notification(self, task_id, time):
        """
            Remove task notification

            Args:
                task_id: handling task id
                time: notification time, format %Y-%m-%d %H:%M

        """

        notifications = self.storage.get_task_field(task_id, notifications=True)
        if time in notifications:
            notifications.remove(time)
        self.storage.update_task_field(task_id, notifications=notifications)


    def get_nearest_notification(self, task_id):
        """
            Get mininal time notification.

            Args:
                task_id: handling task id

            Returns:
                nearest notification time

        """

        if self.storage.get_task(task_id).status is Status.ACTIVE:
            notifications = self.storage.get_task_field(task_id, notifications=True)
            if notifications:
                return min(notifications)
            else:
                return None


    def get_task(self, task_id):
        """
            Return task object from storage.

            Args:
                task_id: task id

        """
        return self.storage.get_task(task_id)


    def update_finish_time(self, task_id, time):
        """
            Update task finish time, remove old finish time notification and add new.
            Args:
                task_id: handling task id
                time: new finish time

        """

        task = self.storage.get_task(task_id)
        self.remove_notification(task_id, task.finish)
        task.finish = time
        self.add_notification(task_id, task.finish)
        self.storage.update_task(task_id, task)


    def get_root_task(self, task_id):
        """
            Get main task on tasks tree, main task doesn't have parent task.

            Args:
                task_id: handling task id

            Returns:
                root task id

        """

        task = self.storage.get_task(task_id)
        return (task_id if task.parent_task is None
                else self.get_root_task(task.parent_task))


    def set_subtask(self, parent_task_id, task_id):
        """
            Set task-subtask relations between tasks.
            Task can't finished before all tasks finishing.

            Args:
                parent_task_id: handling task id
                task_id: handling task id

            Raises:
                IncorrectRelations exception, cycle subtasks and others

        """

        task = self.storage.get_task(task_id)
        if self.get_root_task(parent_task_id) == self.get_root_task(task_id):
            raise exceptions.IncorrectRelations(parent_task_id, task_id)
        task.parent_task = parent_task_id
        self.storage.update_task(task_id, task)


    def set_task_properties(self, task_id, title=None, finish=None, priority=None, status=None):
        """
            Update task propertis.

            Args:
                task_id: handling task id
                title: task title
                finish: task finish time
                priority: task priority, lib.entity.task.Priority object
                status: task status, lib.entity.task.Status object

        """

        task = self.storage.get_task(task_id)
        task.title = title if title is not None else task.title
        task.finish = finish if finish is not None else task.finish
        task.priority = priority if priority is not None else task.priority
        task.status = status if status is not None else task.status
        self.storage.update_task(task_id, task)


    def remove_outdated_notifications(self, task_id):
        """
            Remove tasks outdated notifications.

            Args:
                task_id: handling task id

            Returns:
                remaining notifications number

        """

        notifications = self.storage.get_task_field(task_id=task_id, notifications=True)
        cur_time = datetime.now().strftime('%Y-%m-%d %H:%M')
        notifications = list(filter(lambda n: n >= cur_time, notifications))
        self.storage.update_task_field(task_id=task_id, notifications=notifications)
        return len(notifications)


    def validate_permissions(self, user_id, task_id, operation):
        """
            Check if user has access for the task

            Args:
                user_id: user id
                task_id: handling task id
                operation: checking operation type, lib.entity.task.OperationType object

            Returns:
                True if operation is allowed, else False
        """

        permissions = self.storage.get_task_permissions(user_id, task_id)
        return operation in permissions


    def add_task(self, user_id, task_id, user_rights=Permissions.ADMIN):
        """
            Add task to user, set user permissions for added task.

            Args:
                user_id: user
                task_id: handling task id
                user_rights: user access rights, list() lib.entity.tasks.OperationType objects

        """
        if isinstance(user_rights, Permissions):
            user_rights = user_rights.value

        self.storage.set_task_permissions(user_id, task_id, user_rights)
        message = 'user {0} get access to task {1}'.format(user_id, task_id)
        get_logger().info(message)
        self.log_task_action(task_id, message)


    def log_task_action(self, task_id, message):
        """
            Save task action to task history.

            Args:
                task_id: handling task id
                message: tasl action

        """

        history = self.storage.get_task_field(task_id, history=True)
        time = datetime.now().strftime('%Y-%m-%d %H:%M')
        history.append(TaskActionRecord(time, message))
        self.storage.update_task_field(task_id, history=history)
    # endregion tasks_logic

    # region plans_logic
    def create_plan(self, user_id, title, starting_date,
                    time_delta=None, weekdays=None, repeat=None, extreme_date=None):
        """
            Create new user plan.

            Args:
                user_id: user creator id
                title: plan and task name
                starting date: date for first task adding
                time_delta: difference between plan days, default(None)
                weekdays: days of week for plan, default(None)
                repeat: how many times plan will add task, default(None)
                extreme_date: extreme date for task adding, datetime object

            Returns:
                added plan id

        """

        plan = Plan(user_id=user_id, title=title, time_delta=time_delta,
                     weekdays=weekdays, repeat=repeat, extreme_date=extreme_date,
                     id=self.gen_id())
        if weekdays is None and time_delta is None:
            return
        get_logger().info('user {0} created plan {1}'.format(user_id, plan.id))
        if datetime.strptime(starting_date, '%Y-%m-%d %H:%M') < datetime.now():
            plan.last_added = starting_date
            self.storage.update_plan(plan.id, plan)
            self.update_plan_task(plan.id)
        else:
            self.storage.update_plan(plan.id, plan)
            self.add_plan_task(plan.id, starting_date)
        return plan.id


    def add_plan_task(self, plan_id, next_date=None):
        """
            Add next plan task to user.

            Args:
                plan_id: id of plan-worker
                next_date: next adding task finish date

            Returns:
                added task id

        """

        plan = self.storage.get_plan(plan_id)
        plan.last_added = next_date
        if plan.extreme_date is not None:
            if next_date > plan.extreme_date:
                return
        if plan.repeat is not None:
            if plan.repeat <= 0:
                return
            else:
                plan.repeat -= 1
        task_id = self.create_task(plan.title, next_date, creator=plan.user_id)
        message = 'plan {0} added task {1}'.format(plan_id, task_id)
        self.log_task_action(task_id, message)
        get_logger().info(message)
        self.add_task(plan.user_id, task_id)
        plan.last_added_id = task_id
        self.storage.update_plan(plan.id, plan)
        return task_id


    def remove_plan(self, user_id, plan_id):
        """
            Remove user task plan.

            Args:
                user_id: plan owner id
                plan_id: plan id for removing

            Raises:
                PermissionDenied exception
        """

        plan = self.storage.get_plan(plan_id)
        if plan.user_id != user_id:
            raise exceptions.PermissionDenied(user_id, plan_id)
        self.storage.update_plan(plan_id)
        get_logger().info('user {0} removed plan {1}'.format(user_id, plan_id))


    def update_plan_task(self, plan_id):
        """
            Add next task, if previous plan task is outdated.

            Args:
                plan_id: id of checking plan

            Returns:
                last added task id

        """

        plan = self.storage.get_plan(plan_id)
        if (plan.last_added_id is not None and
                self.storage.get_task(plan.last_added_id).status == Status.ACTIVE):
            return plan.last_added_id

        cur_time = datetime.now()
        next_date = datetime.strptime(plan.last_added, '%Y-%m-%d %H:%M')
        if plan.weekdays is None:
            next_date = next_date + plan.time_delta
            while next_date < cur_time:
                next_date = next_date + plan.time_delta
        else:
            next_date = next_date + timedelta(days=1)
            while next_date < cur_time or plan.weekdays[next_date.weekday()] != '1':
                next_date = next_date + timedelta(days=1)
        next_date = next_date.strftime('%Y-%m-%d %H:%M')

        return self.add_plan_task(plan_id, next_date)
    # endregion plans_logic

    # region tasks_updater
    def update_tasks_status(self, user_id=None):
        """
            Update tasks status, marked task failed.

            Args:
                user_id: update concrete user tasks, default(None)
        """

        tasks_list = self.storage.get_filtered_tasks(user_reader=user_id, status=Status.ACTIVE)
        for task_id in tasks_list:
            self.remove_outdated_notifications(task_id)
            task = self.storage.get_task(task_id)
            if not self.storage.get_task_field(task_id, notifications=True):
                task.status = Status.FAILED
            self.storage.update_task(task_id, task)


    def update_plans(self, user_id=None):
        """
            Replaced outdated plan tasks by new plan tasks.

            Args:
                user_id: update concrete user tasks, default(None)
        """

        plans_list = self.storage.get_plans(user_id=user_id)
        for plan_id in plans_list:
            self.update_plan_task(plan_id)
    #endregion tasks_updater