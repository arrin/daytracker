"""
    Abstract storage implementation for data storage

    Usage examples:

        Creating storage implementation, based on Abstarct Storage::
        >>> class SQLiteStorage(AbstractStorage):
        >>>        pass
"""


from abc import ABCMeta, abstractmethod


class AbstractStorage(metaclass=ABCMeta):
    @abstractmethod
    def clear(self):
        pass

    @abstractmethod
    def get_task_permissions(self, user_id, task_id):
        """
            Returns:
                 list with OperationType()

        """
        pass

    @abstractmethod
    def set_task_permissions(self, user_id, task_id, user_permissions):
        pass

    @abstractmethod
    def get_task(self, task_id):
        pass

    @abstractmethod
    def upload_task(self, task):
        pass

    @abstractmethod
    def remove_task(self, task_id):
        pass

    @abstractmethod
    def get_task_field(self, task_id, history, notifications, informations):
        """
            Returns:
                 history: list with TaskActionRecord()
                 notifications: list with notifications in str format('%Y-%m-%d %h:%m')
                 informations: list with task_id informations relations

        """
        pass

    @abstractmethod
    def update_task_field(self, task_id, history, notifications, informations):
        pass

    @abstractmethod
    def upload_plan(self, plan):
        pass

    @abstractmethod
    def get_plan(self, plan_id):
        pass

    @abstractmethod
    def remove_plan(self, plan_id):
        pass

    @abstractmethod
    def get_user_plans(self, user_id):
        """
            Returns:
                 list with plans ids owned by user

        """
        pass

    @abstractmethod
    def get_user_list(self, user_id, list_name):
        """
            Returns:
                 list with task_id that in user_list

        """
        pass

    @abstractmethod
    def update_user_list(self, user_id, list_name, tasks):
        pass

    @abstractmethod
    def get_filtered_tasks(self, user_reader, status, title, priority,
                            created, finish, parent_task):
        """
            Returns:
                list with task ids

        """
        pass 
