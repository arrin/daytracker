"""
    Provides interface for data storing.

    Modules:
        abstract_storage - abstract storage implementation 
        json_storage - implementation of json storage

"""
