"""
    Json storage implementation

    Classes:
        JSONStorage - json storage

"""

import os
import copy
from shutil import rmtree
from json_tricks import dump, load
from lib import exceptions
from lib.entity.task import OperationType
from lib.storage.abstract_storage import AbstractStorage


class JSONStorage(AbstractStorage):
    def __init__(self, path):
        """
            Args:
                path: path to folder for json-storage
        """

        self.path = path

        self.tasks_list_path = os.path.join(os.path.expanduser(self.path), 'tasks_list')
        if not os.path.exists(self.tasks_list_path):
            serialize(set(), self.tasks_list_path)

        self.plans_list_path = os.path.join(os.path.expanduser(self.path), 'plans_list')
        if not os.path.exists(self.plans_list_path):
            serialize(set(), self.plans_list_path)

        self.tasks_path = os.path.join(os.path.expanduser(self.path), 'task')
        if not os.path.exists(self.tasks_path):
            os.mkdir(self.tasks_path)

        self.plans_path = os.path.join(os.path.expanduser(self.path), 'plan')
        if not os.path.exists(self.plans_path):
            os.mkdir(self.plans_path)

        self.lists_path = os.path.join(os.path.expanduser(self.path), 'user_lists')
        if not os.path.exists(self.lists_path):
            os.mkdir(self.lists_path)


    def clear(self):
        """
            Clear storage.
        """

        os.remove(self.tasks_list_path)
        os.remove(self.plans_list_path)
        rmtree(self.tasks_path)
        rmtree(self.plans_path)
    
    
    def get_task_permissions(self, user_id, task_id):
        """
            Get user permissions for task.

            Args:
                user_id: user for check
                task_id: handling task id

            Returns:
                list with user permissions

        """

        storage_path = os.path.join(self.tasks_path, task_id)
        try:
            task = deserialize(storage_path)
        except Exception:
            raise exceptions.TaskNotFound(task_id)
        return task.users[user_id] if user_id in task.users else list()
    
    
    def set_task_permissions(self, user_id, task_id, user_permissions):
        """
            Set user permissions for task.

            Args:
                user_id: user for update
                task_id: handling task id
                permissions: mask with user permissions
                
        """

        storage_path = os.path.join(self.tasks_path, task_id)
        try:
            task = deserialize(storage_path)
            task.users[user_id] = user_permissions
        except Exception:
            raise exceptions.TaskNotFound(task_id)
        serialize(task, storage_path)  
    
    
    def get_task(self, task_id):
        """
            Task getter

            Args:
                task_id: required task id

            Returns:
                task, lib.entity.Task object

        """

        storage_path = os.path.join(self.tasks_path, task_id)
        try:
            task = deserialize(storage_path)
        except Exception:
            raise exceptions.TaskNotFound(task_id)
        return task
    
    
    def upload_task(self, task):
        """
            Update task on storage, create it if task doesn't exist.

            Args:
                task: task to update
        """

        storage_path = os.path.join(self.tasks_path, task.id)
        serialize(task, storage_path)
        tasks = deserialize(self.tasks_list_path)
        tasks.add(task.id)
        serialize(tasks, self.tasks_list_path)
    
    
    def remove_task(self, task_id):
        """
            Remove task from storage with its dependencies.

            Args:
                task_id: task to remove

        """

        storage_path = os.path.join(self.tasks_path, task_id)
        try:
            os.remove(storage_path)
        except Exception:
            raise exceptions.TaskNotFound(task_id)
            
        tasks = deserialize(self.tasks_list_path)
        tasks.remove(task_id)
        serialize(tasks, self.tasks_list_path)

        dependent_tasks = self.get_filtered_tasks(parent_task=task_id) 

        for task_id in dependent_tasks:
            task = self.get_task(task_id)
            task.parent_task = None
            self.upload_task(task)
    
    
    def get_task_field(self, task_id, history, notifications, informations):
        """
            Get task actions records.

            Args:
                task_id: handling task id

        """

        task = self.get_task(task_id)

        if history is True:
            return task.history
        elif notifications is True:
            return task.notifications
        elif informations is True:
            return list(task.informations)
        else:
            return None
    
    
    def update_task_field(self, task_id, history, notifications, informations):
        """
            Update task actions history.

            Args:
                task_id: handling task id
                history: new task history, list() object
        """

        task = self.get_task(task_id)

        if history is not None:
            task.history = history
        elif notifications is not None:
            task.notifications = notifications
        elif informations is not None:
            task.informations = set(informations)
        self.upload_task(task)
    
    
    def upload_plan(self, plan):
        """
            Update plan on storage, create it if plan doesn't exist.

            Args:
                plan: plan to update

        """

        storage_path = os.path.join(self.plans_path, plan.id)
        serialize(plan, storage_path)
        plans = deserialize(self.plans_list_path)
        plans.add(plan.id)
        serialize(plans, self.plans_list_path)
    
    
    def get_plan(self, plan_id):
        """
            User plan getter.

            Args:
                plan_id: getting plan id
            
            Returns:
                plan, lib.entity.Plan object

        """

        storage_path = os.path.join(self.plans_path, plan_id)
        try:
            plan = deserialize(storage_path)
        except Exception:
            raise exceptions.PlanNotFound(plan_id)
        return plan


    def get_user_plans(self, user_id):
        """
            Get user plans list.

            Args:
                user_id: user

            Returns:
                list user plans ids

        """

        plans_ids = deserialize(self.plans_list_path)
        user_plans_list = list()
        for plan_id in plans_ids:
            if (self.get_plan(plan_id).user_id == user_id or user_id is None):
                user_plans_list.append(plan_id)
        return user_plans_list


    def remove_plan(self, plan_id):
        """
            Remove plan from storage.

            Args:
                plan_id: plan to remove

        """

        storage_path = os.path.join(self.plans_path, plan_id)
        try:
            os.remove(storage_path)
        except Exception:
            raise exceptions.PlanNotFound(plan_id)

        plans = deserialize(self.plans_list_path)
        plans.remove(plan_id)
        serialize(plans, self.plans_list_path)


    def init_user_lists(self, user_id, user):
        """
            Add user lists object to storage, call it when user created

            Args:
                user_id: user id
                user: user object to upload

        """

        storage_path = os.path.join(self.lists_path, user_id)
        try:
            deserialize(storage_path)
        except Exception:
            serialize(user, storage_path)


    def get_user_list(self, user_id, list_name=None):
        """
            Get tasks on concrete user list or all user lists, if list_name is None

            Args:
                user_id: user
                list_name: user list name, default(None) all user lists

        """

        storage_path = os.path.join(self.lists_path, user_id)
        try:
            user_lists = deserialize(storage_path)
        except Exception:
            raise exceptions.UserNotFound(user_id)

        if list_name is not None:
            if list_name in user_lists.lists:
                return list(user_lists.lists[list_name])
            else:
                raise exceptions.ListNotFound(list_name)
        else:
            return user_lists.lists.keys()    


    def update_user_list(self, user_id, list_name, tasks=None):
        """
            Update user list by another tasks list, if tasks is None delete user list

            Args:
                user_id: user
                list_name: user list name
                tasks: new tasks list

        """

        storage_path = os.path.join(self.lists_path, user_id)
        try:
            user_lists = deserialize(storage_path)
        except Exception:
            raise exceptions.UserNotFound(user_id)
        
        if tasks is None:
            del user_lists.lists[list_name]
        else:
            user_lists.lists[list_name] = set(tasks)
        serialize(user_lists, storage_path)


    def get_filtered_tasks(self, user_reader=None, status=None, title=None, priority=None, 
                            created=None, finish=None, user_creator=None, parent_task=-1):
        """
            Get task filtered by values

            Args:
                status: task status
                title: task title
                priority: task priority
                created: task creation date
                finish: task finish date
                user_creator: task creator id
                parent_task: parent task, None if task has not parent

        """

        task_ids = deserialize(self.tasks_list_path)
        task_ids = (task_ids if status is None else self
            ._filter(task_ids, lambda t: t.status == status))
        task_ids = (task_ids if title is None else self
            ._filter(task_ids, lambda t: t.title == title))
        task_ids = (task_ids if priority is None else self
            ._filter(task_ids, lambda t: t.priority == priority))
        task_ids = (task_ids if created is None else self
            ._filter(task_ids, lambda t: t.created[:10] == created))
        task_ids = (task_ids if finish is None else self
            ._filter(task_ids, lambda t: t.finish[:10] == finish))
        task_ids = (task_ids if user_creator is None else self
            ._filter(task_ids, lambda t: t.user_creator == user_creator))
        task_ids = (task_ids if parent_task == -1 else self
            ._filter(task_ids, lambda t: t.parent_task == parent_task))

        task_suite = list()
        if user_reader is not None:
            for task_id in task_ids:
                if OperationType.READ in self.get_task_permissions(user_reader, task_id):
                    task_suite.append(task_id)
        else:
            task_suite = task_ids

        return task_suite


    def _filter(self, tasks, key):
        task_suite = list()
        for task_id in tasks:
            if key(self.get_task(task_id)):
                task_suite.append(task_id)
        return task_suite



def serialize(instance=None, path=None):
    """Serialize object to json-format and save in path file.

    Args:
        instance: instance for serialization
        path: path for saving

    """

    with open(path, "w") as f:
        dump(instance, fp=f, indent=4)


def deserialize(path=None):
    """Deserialize object on file and returns it.

    Args:
        path: path for loading

    Returns:
        deserialized object
    
    """

    with open(path, "r") as f:
        data = load(f)
    return data