"""
    Library exceptions.

"""


class UserNotFound(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'There is no such user as {0.value}.'.format(self)


class PlanNotFound(Exception):
    def __init__(self, plan_id):
        self.value = plan_id

    def __str__(self):
        return 'There is no such task {0.value}.'.format(self)


class TaskNotFound(Exception):
    def __init__(self, task_id):
        self.value = task_id

    def __str__(self):
        return 'There is no such task {0.value}.'.format(self)


class PermissionDenied(Exception):
    def __init__(self, user_id, id):
        self.user = user_id
        self.value = id

    def __str__(self):
        return ('User {0} can\'t access {1}.'.format(self.user, self.value))


class ListNotFound(Exception):
    def __init__(self, list_name):
        self.name = list_name

    def __str__(self):
        return ('List {0} not found.'.format(self.name))


class IncorrectRelations(Exception):
    def __init__(self, first_task, second_task):
        self.first_task = first_task
        self.second_task = second_task

    def __str__(self):
        return ('Incorrect relations between task {0} and task {1}.'
                .format(self.first_task, self.second_task))


class ImpossibleToFinish(Exception):
    def __init__(self, task_id, msg):
        self.task_id = task_id
        self.message = msg

    def __str__(self):
        return ('Impossible to finish task {0}. {1}'
                .format(self.task_id, self.message))            


class InvalidConfig(Exception):
    def __str__(self):
        return 'Correct config not found.'


class StorageNotFound(Exception):
    def __str__(self):
        return 'Storage not found.'