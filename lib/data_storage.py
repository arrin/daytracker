"""
    Interface for application data interaction.

    Classes:
        Storage: interface for storage implementation, provides functions for tasks manager

    Usage examples:
        JSON-Storage creation, set up storage path before::
            >>> from lib.storage.json_storage import JSONStorage
            >>> json_storage = JSONStorage(path)

        Storage interface creation, set up storage implementation before::
            >>> from lib.data_storage import Storage
            >>> storage = Storage(json_storage)

        Add new task to storage::
            >>> task = Task()
            >>> storage.update_task(task.id, task)

        Getting filtered tasks::
            >>> storage.get_filtered_tasks(title='tested', finish_date='2018-07-23'))

        Get all existent plans::
            >>> storage.get_plans()

        Get all user plans::
            >>> storage.get_plans(user_id=username)

"""
 
from lib import exceptions
from lib.entity.user_lists import UserLists
from lib.storage.abstract_storage import AbstractStorage
from lib.entity.task import Priority, Status


class Storage:
    """
        Class with application self instance.
 
        Args:
            storage: pointer to storage implementation

    """

    def __init__(self, storage_instance):
        """
            Args:
                storage_instance: storage implementation

        """
        if not isinstance(storage_instance, AbstractStorage):
            raise TypeError('Storage instance is not storage implementation.')

        self.storage = storage_instance
    

    def clear(self):
        """
            Clear application storage.
                
        """

        self.storage.clear()


    # region task permissions
    def get_task_permissions(self, user_id, task_id):
        """
            Get user permissions for task.

            Args:
                user_id: user for check
                task_id: handling task id

            Returns:
                mask with user permissions

        """

        permissions = self.storage.get_task_permissions(user_id, task_id)
        return permissions


    def set_task_permissions(self, user_id, task_id, permissions):
        """
            Set user permissions for task.

            Args:
                user_id: user for update
                task_id: handling task id
                permissions: mask with user permissions
                
        """

        self.storage.set_task_permissions(user_id, task_id, permissions)
    # endregion

    # region task handling
    def get_task(self, task_id):
        """
            Task getter

            Args:
                task_id: required task id

            Returns:
                task, lib.entity.Task object

        """

        return self.storage.get_task(task_id)


    def update_task(self, task_id, task=None):
        """
            Update task, placed on task id, if task is None delete task.

            Args:
                task_id: handling plan id
                tsak: lib.entity.Task object
                
        """

        if task is not None:
            self.storage.upload_task(task)
        else:
            self.storage.remove_task(task_id)


    def get_task_field(self, task_id, history=False, notifications=False, informations=False):
        """
            Get task related field.

            Args:
                task_id: nadling task id
                history: true if you need task history
                notifications: true if you need task notifications

        """

        return self.storage.get_task_field(task_id, history, notifications, informations)


    def update_task_field(self, task_id, history=None, notifications=None, informations=None):
        """
            Update task related field.

            Args:
                task_id: nadling task id
                history: history list for update
                notifications: notifications list for update
        """

        self.storage.update_task_field(task_id, history, notifications, informations)
    # endregion


    # region plans handling
    def get_plans(self, user_id=None):
        """
            Get plans list, owned by user, if user_id is None return all plans

            Args:
                user_id: user

            Returns:
                list user plans ids

        """

        return self.storage.get_user_plans(user_id)


    def get_plan(self, plan_id):
        """
            User plan getter.

            Args:
                plan_id: getting plan id
            
            Returns:
                plan, lib.entity.Plan object

        """

        return self.storage.get_plan(plan_id)


    def update_plan(self, plan_id, plan=None):
        """
            Update user plan, placed on plan id, if plan is None delete plan.

            Args:
                plan_id: handling plan id
                plan: lib.entity.Plan object
                
        """

        if plan is not None:
            self.storage.upload_plan(plan)
        else:
            self.storage.remove_plan(plan_id)
    # endregion

    # region user lists handling
    def init_user_lists(self, user_id, lists=UserLists()):
        """
            Add new user to system.

            Args:
                user_id: user
                user: user lists object

        """

        self.storage.init_user_lists(user_id, lists)


    def get_user_lists_names(self, user_id):
        """
            Get all user lists names.

            Args: 
                user_id: user
        """

        return self.storage.get_user_list(user_id)


    def get_user_list(self, user_id, list_name):
        """
            Get tasks on concrete user list

            Args:
                user_id: user
                list_name: user list name
        """

        return self.storage.get_user_list(user_id, list_name)


    def update_user_list(self, user_id, list_name, tasks=None):
        """
            Update user list by another tasks list, if tasks is None delete user list

            Args:
                user_id: user
                list_name: user list name
                tasks: new tasks list

        """

        self.storage.update_user_list(user_id, list_name, tasks)
    # endregion 

    # region all tasks matching
    def get_filtered_tasks(self, user_reader=None, status=None, title=None, priority=None,
                           created_date=None, finish_date=None, user_creator=None, parent_task=-1):
        """
            Get task filtered by values

            Args:
                status: task status
                title: task title
                priority: task priority
                created_date: task creation date, format (Y-m-d)
                finish_date: task finish date, format (Y-m-d)
                user_creator: task creator id
                parent_task: parent task, None if task has not parent

        """
        if not isinstance(status, Status) and status is not None:
            status = Status(int(status))
        if not isinstance(priority, Priority) and priority is not None:
            priority = Priority(int(priority))

        return self.storage.get_filtered_tasks(user_reader, status, title, priority, created_date,
                                               finish_date, user_creator, parent_task)
    # endregion