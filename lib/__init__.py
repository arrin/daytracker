"""
    This package includes main daytracker library.

    Packages:
        entity - entites, used on lib
        storage - provides function for data interaction

    Modules:
        exceptions - lib exceptions
        logger - logger interface
        tasks_manager - core logic module, store class logic for tasks handling

    Library classes:
        JSONStorage - json storage implementation, use as default implementation
        Storage - interface for storage implementation
        TasksManager - core logic class 

    Usage examples.
        JSON-Storage creation, set up storage path before::

            >>> from lib.storage.json_storage import JSONStorage
            >>> json_storage = JSONStorage(path)

        Storage interface creation, set up storage implementation before::

            >>> from lib.data_storage import Storage
            >>> storage = Storage(json_storage)

        TasksManager logic creation, use storage implementation, Storage class::

            >>> from lib.tasks_manager import TasksManager
            >>> manager = TasksManager(storage)
            ...      Use help(manager) for more functions.

        Task creating example::

            >>> task_id = manager.create_task('another_task', '2018-06-16 12:00')
            ...     Task id is a unique string for tasks handling. 

"""