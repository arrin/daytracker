"""
    Provides logger handling functions.

"""

import logging


def get_logger():
    """Returns library application logger."""

    return logging.getLogger('daytracker-build06.18.2')


def disable_logger():
    """Disable application logger."""

    get_logger().disabled = True


def enable_logger():
    """Enable application logger."""

    get_logger().disabled = False

