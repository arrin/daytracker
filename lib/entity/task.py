"""
    Provides task class with task information.

    Classes:
        Task() - task class, store information about tasks
        Status() - task ready status
        Priority() - tasks priority
        OperationType() - operations types for tune user permissions
        TaskActionRecord() - task action record
        Permissions() - class with most used permissions types

"""

from enum import Enum
from uuid import uuid4


class Task():
    """
        Task information class

        Args:
            status: current task status, task.Status object
            title: brief task description
            priority: task priority, task.Priority object
            created: task creation time
            finish: task finish time
            id: unique task id
            parent_task: id of task ancestor
            notifications: task notifications time
            users: users and their permissions for this task
            informations: task informations relations
            user_creator: user creator id

    """
    
    def __init__(self,
                 status=None,
                 title=None,
                 priority=None,
                 created=None,
                 finish=None,
                 id=None,
                 user_creator=None,
                 parent_task=None,
                 notifications=None,  # another table
                 users=None,          # another table
                 history=None,          # another table
                 informations=None):  # another table 
        
        self.status = status if status is not None else Status.ACTIVE
        self.priority = priority if priority is not None else Priority.NORMAL
        self.notifications = notifications if notifications is not None else list()
        self.users = users if users is not None else dict()
        self.history = history if history is not None else list()
        self.informations = informations if informations is not None else set()
        self.title = title
        self.created = created
        self.finish = finish
        self.id = id if id is not None else str(uuid4().hex)[-5:]
        self.parent_task = parent_task
        self.user_creator = user_creator


class Status(Enum):
    """
        Information about task status.

        Values:
            ACTIVE: task is active, user will get notifications, value=2
            FINISHED: task was finsihed, value=1
            FAILED: value=0
    """

    ARCHIVED = 3
    ACTIVE = 2
    FINISHED = 1
    FAILED = 0

    def __str__(self):
        return str(self.name).lower()


class Priority(Enum):
    """
        Information about task priority.
    """

    HIGH = 2
    NORMAL = 1
    LOW = 0

    def __str__(self):
        return str(self.name).lower()



class OperationType(Enum):
    """
        Provides task operations types for tune user permissions.
    """

    READ = 0
    FINISH = 1
    EDIT = 2
    REMOVE = 3

    def __str__(self):
        return str(self.name).lower()


class TaskActionRecord:
    """
        Task action record.

        Args:
            time: action time
            message: action log
    """

    def __init__(self, time, message):
        self.time = time
        self.message = message

    def __str__(self):
        return '{0.time}  -  {0.message}'.format(self)



class Permissions(Enum):
    """
        Class with most used permissions types.

        Values:
            ADMIN: all operations are allowed
            READ: read-only 
    """

    ADMIN = [item for item in OperationType]
    READ = [OperationType.READ]