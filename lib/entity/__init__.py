"""
    Application main entities.

    Modules:
        plan - provides plans for sheduler
        task - task class, store information about tasks
        user_lists - store information about user task groups

"""