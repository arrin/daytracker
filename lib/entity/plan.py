"""
    Provides plan class, that allow adding tasks by some plan(some weekdays or every n weekdays).
    
    Classes:
        Plan() - User plan class

"""

from uuid import uuid4


class Plan():
    """
    User plan class

    Args:
        weekdays: weekdays for adding tasks, higher priority
        time_delta: cycle for adding tasks, lower priority
        title: plan and adding task title
        last_added: last added task time
        last_added_id: last added task id
        id: plan unique id
        repeat: how many times plan will add task
        extreme_date: extreme date for task adding, datetime type
        user_id: plan creator user id

    """

    def __init__(self, 
                 weekdays=None,
                 time_delta=None,
                 title=None,
                 last_added=None,
                 last_added_id=None,
                 id=None,
                 repeat=None,
                 extreme_date=None,
                 user_id=None):

        self.id = id if id is not None else str(uuid4().hex)[-5:]
        self.time_delta = time_delta
        self.weekdays = weekdays
        self.title = title
        self.last_added = last_added
        self.repeat = repeat
        self.extreme_date = extreme_date
        self.user_id = user_id
        self.last_added_id = last_added_id 