"""
    Provides class for storing user tasks groups.

    Classes:
        UserLists() - user class, storing information about user lists.

"""

from uuid import uuid4


class UserLists:
    """
        Information about user lists, names and task ids.

        Args:
            id: list owner user id
            lists: dict with user lists, store list_name, tasks on list

    """

    def __init__(self,
                 id=None,
                 lists=None):
        
        self.id = id if id is not None else str(uuid4().hex)[-5:]
        self.lists = lists if lists is not None else dict()